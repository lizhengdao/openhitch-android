package com.graphhopper.navigation.example.openhitch

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView

import com.graphhopper.navigation.example.R

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.graphhopper.navigation.example.openhitch.navi.ChooseWaypointsActivity
import com.graphhopper.navigation.example.openhitch.service.HitchService
import com.graphhopper.navigation.example.openhitch.service.api.Api
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import timber.log.Timber

class MainActivity : HitchActivity() {
    @BindView(R.id.list_button)
    lateinit var listButton: Button

    @BindView(R.id.infobox)
    lateinit var infobox: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        infobox.text = getText(R.string.welcome_text)
        startService(Intent(this, HitchService::class.java))
    }

    override fun onHitchServiceAttach(api: Api) {
        super.onHitchServiceAttach(api)
        hitchServiceConnection.observe(api.ways.wayListStream({it->true})) {
            updateListButton(it)
        }
    }

    private fun updateListButton(ways:List<Way>) {
        val size = ways.size
        if (size ==0) {
            listButton.visibility = View.GONE
        } else {
            listButton.visibility = View.VISIBLE
            listButton.text = getResources().getQuantityString(R.plurals.ride_list_title, size, size)
        }
    }

    @OnClick(R.id.offer_button)
    fun onOfferButtonClick() {
        val intent = Intent(this, ChooseWaypointsActivity::class.java)
        val extras = Bundle()
        extras.putSerializable(ChooseWaypointsActivity.PARAMETER_ROLE, Role.DRIVER)
        intent.putExtras(extras)
        startActivity(intent)
    }

    @OnClick(R.id.search_button)
    fun onSearchButtonClick() {
        val intent = Intent(this, ChooseWaypointsActivity::class.java)
        val extras = Bundle()
        extras.putSerializable(ChooseWaypointsActivity.PARAMETER_ROLE, Role.PASSENGER)
        intent.putExtras(extras)
        startActivity(intent)
    }

    @OnClick(R.id.list_button)
    fun onListButtonClick() {
        val intent = Intent(this, MyWaysActivity::class.java)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        logState("onStart")
    }

    override fun onResume() {
        super.onResume()
        logState("onResume")
    }

    override fun onPause() {
        super.onPause()
        logState("onPause")
    }

    override fun onStop() {
        super.onStop()
        logState("onStop")
    }

    override fun onRestart() {
        super.onRestart()
        logState("onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        logState("onDestroy")
    }

    fun logState(state: String){
        Timber.d("MainActivity state %s",state)
    }
}
