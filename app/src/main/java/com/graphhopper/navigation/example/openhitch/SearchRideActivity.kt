package com.graphhopper.navigation.example.openhitch

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.Spinner
import android.widget.TextView

import com.graphhopper.navigation.example.R

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.lang.RuntimeException
import java.util.Date
import kotlin.collections.ArrayList

//import java.util.*

class SearchRideActivity : HitchActivity() {
    private var previousStartTime: Int = -1

    @BindView(R.id.all)
    lateinit var rootView: View

    @BindView(R.id.required_seats_spinner)
    lateinit var requiredSeatsSpinner: Spinner
    @BindView(R.id.start_point_view)
    lateinit var startPointTextView: TextView
    @BindView(R.id.destination_point_view)
    lateinit var destinationPointTextView: TextView
    @BindView(R.id.date_button)
    lateinit var dateButton: DateButton
    @BindView(R.id.start_time_button)
    lateinit var startTimeButton: TimeButton
    @BindView(R.id.end_time_button)
    lateinit var endTimeButton: TimeButton
    @BindView(R.id.submit_button)
    lateinit var submitButton: Button
    @BindView(R.id.next_day_view)
    lateinit var nextDayView: TextView
    @BindView(R.id.cb_auto_request)
    lateinit var cbAutoRequest: CheckBox
    @BindView(R.id.cb_sectional_match)
    lateinit var cbSectionalMatch: CheckBox

    @BindView(R.id.collision_list_header)
    lateinit var collisionListHeader: TextView;

    lateinit var collisionListFragment: CollisionListFragment;

    private var waypoints = ArrayList<OHPoint>()
    private var duration: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_ride)
        ButterKnife.bind(this)
        collisionListFragment = supportFragmentManager.findFragmentById(R.id.collision_list_fragment) as CollisionListFragment

        getParameters()
        initUi()
        updateCollisionList()
    }

    fun updateCollisionList() {
        val startTime = dateButton.time + startTimeButton.timeOfDay
        val endTime = if(endTimeButton.timeOfDay>startTimeButton.timeOfDay) {
            dateButton.time + endTimeButton.timeOfDay
        } else {
            dateButton.time + endTimeButton.timeOfDay + DAY
        }
        hitchServiceConnection.whenReady { api->
            val observable = api.ways.collisionListStream(startTime, endTime).take(1)
            hitchServiceConnection.observe(observable) {
                collisionListFragment.updateItems(it)
                val n = it.size
                if(n==0) {
                    collisionListHeader.visibility = View.GONE
                } else {
                    collisionListHeader.text = "⚠️"+getResources().getQuantityString(R.plurals.collision_warning_search,n)+":"
                    collisionListHeader.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun initUi() {
        initFreeSeatSpinner()
        initLocations()
        val now = Date()
        dateButton.init(now)
        val timeOfDay = Util.getTimeOfDay(now)
        startTimeButton.timeOfDay = timeOfDay
        previousStartTime = timeOfDay

        val timeWindowDuration = ((duration / HOUR + 3) * HOUR).toInt()
        endTimeButton.timeOfDay = (timeOfDay+ timeWindowDuration)%DAY
        updateNextDayView()
        endTimeButton.listener = {
            updateNextDayView()
            updateCollisionList()
        }
        startTimeButton.listener = {
            dragEndTimeWithStartTime()
        }
    }

    private fun dragEndTimeWithStartTime() {
        var delta = startTimeButton.timeOfDay-previousStartTime
        endTimeButton.timeOfDay += delta
        previousStartTime = startTimeButton.timeOfDay
    }

    private fun initLocations() {
        startPointTextView.text = waypoints[0].description
        destinationPointTextView.text = Util.getLast(waypoints).description
    }

    private fun getParameters() {
        @Suppress("UNCHECKED_CAST")
        waypoints = intent.getSerializableExtra(PARAMETER_WAYPOINTS) as ArrayList<OHPoint>
        duration = intent.getLongExtra(OfferActivity.PARAMETER_DURATION,-1L)
        if (duration==-1L) {
            throw RuntimeException("Route duration not given")
        }
    }

    private fun initFreeSeatSpinner() {
        val items = arrayOf(1, 2, 3, 4, 5, 6, 7, 8)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, items)
        requiredSeatsSpinner.adapter = adapter
    }

    @OnClick(R.id.cancel_button)
    internal fun cancel() {
        finish()
    }

    fun updateNextDayView() {
        if (endTimeButton.timeOfDay<=startTimeButton.timeOfDay) {
            nextDayView.text = getText(R.string.next_day)
        } else {
            nextDayView.text = ""
        }
    }

    @OnClick(R.id.submit_button)
    internal fun searchRideButtonClick() {
        hitchServiceConnection.whenReady { api ->
            val user = api.authentication.user
            if (user == null) {
                val intent = Intent(this, AccountActivity::class.java)
                startActivityForResult(intent,AccountActivity.REQUEST_REGISTER)
            } else {
                submitButton.isEnabled = false
                val way = buildWay(user.id)
                api.createWay(way)
                val intent = Intent(this, WayDetailActivity::class.java)
                intent.putExtra(WayDetailActivity.PARAMETER_WAY_ID, way.id)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(AccountActivity.REQUEST_REGISTER, resultCode, data)
        if (resultCode== Activity.RESULT_OK) {
            Snackbar.make(rootView,R.string.registration_success, Snackbar.LENGTH_LONG).show()
        }
    }


    private fun buildWay(user_id: String): Way {
        val requiredSeats = requiredSeatsSpinner.selectedItem as Int
        val way_id = Util.randomId()
        val startTime = dateButton.time + startTimeButton.timeOfDay // FIXME check: should not be in the past
        var endTime = dateButton.time + endTimeButton.timeOfDay
        if (endTime<startTime) {
            endTime += DAY
        }


        val way = Way(
                id = way_id,
                status = Way.Status.PUBLISHED,
                user_id = user_id,
                role = Role.PASSENGER,
                waypoints = waypoints,
                start_time = startTime,
                end_time = endTime,
                seats = requiredSeats,
                autocommit = cbAutoRequest.isChecked,
                sectional_match = cbSectionalMatch.isChecked
        )
        return way
    }

    companion object {
        val PARAMETER_WAYPOINTS = "waypoints"
        val PARAMETER_DURATION = "duration"
    }
}
