package com.graphhopper.navigation.example.openhitch.service

import com.graphhopper.navigation.example.openhitch.service.api.Api
import com.graphhopper.navigation.example.openhitch.service.api.ApiException
import com.graphhopper.navigation.example.openhitch.service.api.ApiInternal
import de.wifaz.oh.protocol.*
import de.wifaz.oh.protocol.Lift.Status.ACCEPTED
import de.wifaz.oh.protocol.Lift.Status.DRIVER_CANCELED
import de.wifaz.oh.protocol.Lift.Status.PASSENGER_CANCELED
import de.wifaz.oh.protocol.Lift.Status.REJECTED
import de.wifaz.oh.protocol.Lift.Status.REQUESTED
import de.wifaz.oh.protocol.Lift.Status.SUGGESTED
import de.wifaz.oh.protocol.Role.DRIVER
import de.wifaz.oh.protocol.Role.PASSENGER
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.math.BigDecimal
import java.util.*

class Ways(private val api: Api) {

    private val wayContextListStore = object : ListStore<WayContext>() {
        override fun getKey(x: WayContext): String {
            return x.way.id
        }
    }

    fun wayListStream(predicate:(way:Way)->Boolean={true}) : Observable<List<Way>> {
        val p:(wayContext:WayContext)->Boolean = {
            predicate(it.way)
        }
        return wayContextListStore.listStream(p).map() { wayContextList->
            wayContextList.map { it.way }
        }
    }

    fun liftInfoStream(way_id:String, lift_id:String) : Observable<LiftInfo> {
        return getWayContext(way_id).liftInfoListStore.itemStream(lift_id)
    }


    fun wayStream(way_id:String) : Observable<Way> {
        return wayContextListStore.itemStream(way_id).map() {
            wayContext:WayContext -> wayContext.way
        }
    }

    // XXX beware no updates
    fun getWay(way_id: String) : Way {
        return getWayContext(way_id).way
    }

    private fun getWayContext(way_id: String) : WayContext {
        return wayContextListStore.getItem(way_id) ?:  throw ApiException("getWayContext with unknown id: "+way_id)
    }

    fun putWay(way: Way) {
        validateWay(way)
        wayContextListStore.put(WayContext(api,way))
    }

    private fun validateWay(way: Way) {
        way.waypoints.size >= 2 || throw ApiException("Not enough waypoints: "+way.waypoints.size)
    }

    /**
     * Change the status of the way in the local database.
     * It is checked if the status change is legal. When the status
     * is changed to CANCELLED, the way is removed fron the local database.
     *
     * No server interaction is performed. GUI-code should use {@link Api.changeWayStatus} to
     * make the status change also known the server.
     */
    @ApiInternal
    internal fun changeWayStatus(wayId:String, newStatus: Way.Status) {
        try {
            val wayContext = getWayContext(wayId)
            verifyWayStatusChange(wayContext.way.status, newStatus)
            if (newStatus==Way.Status.CANCELED) {
                wayContextListStore.remove(wayId)
            } else {
                val newWay = wayContext.way.copy(status = newStatus)
                putWay(newWay)  //FIXME update UI?
            }
        } catch (e: ApiException) {
            Timber.e(e,"ModelException %s",e.message)
        }
    }

    private fun verifyWayStatusChange(oldStatus: Way.Status, newStatus: Way.Status) {
        val ok = oldStatus==Way.Status.PRELIMINARY && newStatus==Way.Status.RESEARCH
                || oldStatus==Way.Status.RESEARCH && newStatus==Way.Status.PUBLISHED
                || oldStatus==Way.Status.PRELIMINARY && newStatus==Way.Status.PUBLISHED
                || oldStatus==Way.Status.PUBLISHED && newStatus==Way.Status.STARTED
                || oldStatus==Way.Status.STARTED && newStatus==Way.Status.FINISHED
                || oldStatus!=Way.Status.CANCELED && newStatus==Way.Status.CANCELED
        if (!ok) {
            throw ApiException("Incorrect way status change from %s to %s".format(oldStatus.name,newStatus.name))
        }
    }

    fun liftInfoListStream(way_id:String, states: EnumSet<Lift.Status> = EnumSet.allOf(Lift.Status::class.java)) : Observable<List<LiftInfo>> {
        val wayContext = getWayContext(way_id)
        return wayContext.liftInfoListStore.listStream { it.lift.status in states }
    }

    fun liftInfoCountStream(way_id:String, states: EnumSet<Lift.Status> = EnumSet.allOf(Lift.Status::class.java)) : Observable<Int> {
        return liftInfoListStream(way_id,states)
                .map {it.size}
    }

    fun liftInfoCountMapStream(way_id:String) : Observable<Map<Lift.Status,Int?>> {
        return liftInfoListStream(way_id, EnumSet.allOf(Lift.Status::class.java))
                .map {mapCount(it)}
    }

    private fun mapCount(lift_infos: List<LiftInfo>): Map<Lift.Status, Int?> {
        val result = EnumMap<Lift.Status,Int>(Lift.Status::class.java)

        for (liftInfo in lift_infos) {
            val status = liftInfo.lift.status
            result.put(status,(result.get(status)?:0)+1)
        }
        return result
    }

    fun waypointsStream(way_id:String) : Observable<List<OHPoint>> {
        val way = getWayContext(way_id).way
        if (way.role!= DRIVER) {
            throw RuntimeException("streamWaypoints() is only implemented for driver ways")
        }
        return liftInfoListStream(way_id,EnumSet.of(ACCEPTED))
                .map { arrangeWay(way,it) }
    }

    fun collisionListStream(wayId: String) : Observable<List<Way>> {
        val way = getWayContext(wayId).way
        // FIXME make shure that any Fragment using collisionListStream is closed/reloaded on chonge of way status or timing
        return if (way.status in Way.collisionRelevantStates) {
            wayListStream {
                it.id != wayId && it.status in Way.collisionRelevantStates && doCollide(way, it)
            }
        } else {
            Observable.empty()
        }
    }

    fun collisionListStream(startTime: Long, endTime: Long) : Observable<List<Way>> {
        return wayListStream {
            it.status in Way.collisionRelevantStates && doCollide(startTime,endTime,it)
        }
    }

    /**
     * XXX server hints
     */
    private fun arrangeWay(way:Way, lift_infos: List<LiftInfo>) : List<OHPoint> {
        // Pair waypoints and hints temporarily
        class IndexedPoint(var ohPoint: OHPoint, var index: BigDecimal) : Comparable<IndexedPoint> {

            override fun compareTo(other: IndexedPoint): Int {
                return this.index.subtract(other.index).signum()
            }
        }

        // Put theses pairs into a list
        val list = ArrayList<IndexedPoint>()
        for (i in way.waypoints.indices) {
            // Every one of our original waypoints gets its list index as an index
            list.add(IndexedPoint(way.waypoints[i], BigDecimal(i * 10)))
        }
        for (info in lift_infos) {
            if (info.lift.status== ACCEPTED) {
                // The OpenHitch server delivers indizes in between as hints
                list.add(IndexedPoint(info.lift.pick_up_point, info.lift.pick_up_hint))
                list.add(IndexedPoint(info.lift.drop_off_point, info.lift.drop_off_hint))
            }
        }

        // sort them
        Collections.sort(list)

        // throw away the hints and keep an array of points that is sorted according to the hints
        val result = ArrayList<OHPoint>()
        for (ip in list) {
            result.add(ip.ohPoint)
        }
        return result
    }

    fun getLiftInfo(way_id: String, lift_id:String) :LiftInfo {
        val wayContext = getWayContext(way_id)
        return wayContext.liftInfoListStore.getItem(lift_id) ?:  throw ApiException("getLiftInfo with unknown lift_id $lift_id from way ${way_id}")
    }

    /**
     * Change the status of the lift in the local database.
     * It is checked if the status change is legal.
     *
     * No server interaction is performed. GUI-code should use {@link Api.changeLiftStatus} to
     * make the status change also known the server.
     */
    @ApiInternal
    internal fun changeLiftStatus(way_id:String, lift_id: String, newStatus: Lift.Status, byServerMessage:Boolean=false) {
        var liftInfo = getLiftInfo(way_id,lift_id)
        val role = if(byServerMessage) liftInfo.partner_way.role else liftInfo.role
        verifyStatusChange(role, liftInfo.lift.status, newStatus)
        val newLift = liftInfo.lift.copy(status=newStatus)
        val newLiftInfo = liftInfo.copy(lift=newLift)
        putLiftInfo(way_id,newLiftInfo)
    }

    private fun verifyStatusChange(role: Role, oldStatus: Lift.Status, newStatus: Lift.Status) {
        val ok = ( oldStatus== SUGGESTED && newStatus== REQUESTED && role== PASSENGER
                || oldStatus== REQUESTED && newStatus== ACCEPTED && role== DRIVER
                || oldStatus== REQUESTED && newStatus== REJECTED && role== DRIVER
                || oldStatus== ACCEPTED && newStatus== PASSENGER_CANCELED && role== PASSENGER
                || oldStatus== ACCEPTED && newStatus== DRIVER_CANCELED && role== DRIVER)
        if (!ok) {
            throw ApiException("Incorrect status change from %s to %s as %s".format(oldStatus.name,newStatus.name,role.name))
        }
    }

    private fun putLiftInfo(way_id: String, liftInfo: LiftInfo) {
        getWayContext(way_id).liftInfoListStore.put(liftInfo)
    }

    fun cancelLift(wayId: String, liftId: String) {
        val liftInfo:LiftInfo = getLiftInfo(wayId,liftId)
        if (liftInfo.lift.status!= SUGGESTED) {
            var newStatus = when(liftInfo.role) {
                DRIVER -> DRIVER_CANCELED
                PASSENGER -> PASSENGER_CANCELED
            }
            changeLiftStatus(wayId, liftId, newStatus)
        }
    }

    @ApiInternal
    internal fun putLiftInfos(reference_way_id: String, lift_infos: List<LiftInfo>) {
        validateLiftInfos(lift_infos, reference_way_id)
        getWayContext(reference_way_id).liftInfoListStore.putList(lift_infos)
    }

    private fun validateLiftInfos(lift_infos: List<LiftInfo>, reference_way_id: String) {
        for (liftInfo in lift_infos) {
            if (reference_way_id != liftInfo.reference_way_id()) {
                throw ApiException(String.format("Received LiftInfo refers to wrong way: %s instead of %s", liftInfo.reference_way_id(), reference_way_id))
            }
        }
    }


    /**
     * Feeds data returned by HitchInterface.GetUserData into the local database
     */
    @ApiInternal
    internal fun receiveSyncData(syncData:SyncData) {
        wayContextListStore.erase()

        api.authentication.updateUserData(syncData.user)
        for (way in syncData.ways) {
            putWay(way)
        }
        for (liftInfo in syncData.lift_infos) {
            putLiftInfo(liftInfo.reference_way_id(),liftInfo)
        }
    }

    internal fun removeWay(wayId: String) {
        wayContextListStore.remove(wayId)
    }
}

fun doCollide(w1: Way, w2: Way) : Boolean = w1.end_time>w2.start_time && w1.start_time<w2.end_time

fun doCollide(startTime:Long, endTime:Long, w: Way) : Boolean = endTime>w.start_time && startTime<w.end_time

private class WayContext(private val api: Api, val way: Way) {
    val isLoadedSubject: BehaviorSubject<Boolean> = BehaviorSubject.create<Boolean>().apply { onNext(false) }
    val liftInfoListStore = object : ListStore<LiftInfo>() {
        override fun getKey(x: LiftInfo): String {
            return x.lift.id
        }

        override fun internalPut(key: String, x: LiftInfo?) {
            super.internalPut(key, x)
            val liftInfo = x
            when (way.role) {
                DRIVER -> {
                    if (liftInfo?.lift?.status == REQUESTED) {
                        val alert = Alert.Request(way.id, liftInfo.lift.id)
                        api.alerts.put(alert)
                    } else {
                        // FIXME should have alert type in key somehow
                        api.alerts.remove(key)
                    }
                }
                PASSENGER -> {
                    if (liftInfo?.lift?.status == SUGGESTED) {
                        val alert = Alert.Offer(way.id, liftInfo.lift.id)
                        api.alerts.put(alert)
                    } else {
                        api.alerts.remove(key)
                    }
                }
            }
        }
    }



}



