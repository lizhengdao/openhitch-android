package com.graphhopper.navigation.example.openhitch

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker

import java.util.Calendar
import java.util.Date


class TimeButton : androidx.appcompat.widget.AppCompatButton, View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    var listener: (()->Unit)? = null

    /**
     * Shown/choosen time in milliseconds since midnight
     */
    var timeOfDay : Int = 0
        set(value:Int) {
            field = value
            text = Util.formatTime(context,value)
            listener?.invoke()
        }

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    init {
        setOnClickListener(this)
    }

    override fun onClick(view: View) {
        val hour = timeOfDay/HOUR
        val minute = (timeOfDay%HOUR)/MINUTE

        val is24HourFormat = android.text.format.DateFormat.is24HourFormat(context)
        val timePickerDialog = TimePickerDialog(context,this, hour, minute, is24HourFormat)
        timePickerDialog.show()
    }

    override fun onTimeSet(p: TimePicker?, hourOfDay: Int, minute: Int) {
        timeOfDay = hourOfDay*HOUR + minute* MINUTE
    }

}
