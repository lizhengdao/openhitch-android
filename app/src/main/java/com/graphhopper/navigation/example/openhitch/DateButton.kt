package com.graphhopper.navigation.example.openhitch

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.DatePicker
import java.util.*


class DateButton : androidx.appcompat.widget.AppCompatButton, View.OnClickListener, OnDateSetListener {

    lateinit var date: Date
        private set

    val time: Long
        get() = date.time

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    fun init(date: Date) {
        this.date = Util.truncateDate(date)
        text = Util.formatDate(context, this.date)
        setOnClickListener(this)
    }

    override fun onClick(view: View) {
        // Get Current Time
        val c = Calendar.getInstance()
        c.time = Util.truncateDate(date)
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Launch Time Picker Dialog
        val datePickerDialog = DatePickerDialog(context, this, year, month, day);

        datePickerDialog.show()
    }

    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
        val c = Calendar.getInstance()
        c.time = date
        c.set(Calendar.YEAR, year)
        c.set(Calendar.MONTH, month)
        c.set(Calendar.DAY_OF_MONTH, day)
        date = c.time  // FIXME check not in past
        text = Util.formatDate(context, date)
    }
}
