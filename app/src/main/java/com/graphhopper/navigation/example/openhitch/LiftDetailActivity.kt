package com.graphhopper.navigation.example.openhitch

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView

import com.graphhopper.navigation.example.R

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.graphhopper.navigation.example.openhitch.navi.LiftMapFragment
import com.graphhopper.navigation.example.openhitch.service.api.Api
import de.wifaz.oh.protocol.*
import java.util.*

class LiftDetailActivity : HitchActivity() {

    @BindView(R.id.header)
    lateinit var headerView: TextView

    @BindView(R.id.name_view)
    lateinit var nameView: TextView
    @BindView(R.id.pickup_time_view)
    lateinit var pickup_timeView: TextView
    @BindView(R.id.destination_point_view)
    lateinit var destinationPointView: TextView
    @BindView(R.id.partial_lift_warning)
    lateinit var partialLiftWarningView: TextView
    // @BindView(R.id.detour_distance_view)
    // lateinit var detour_distanceView: TextView
    // @BindView(R.id.detour_time_view)
    // lateinit var detour_timeView: TextView
    @BindView(R.id.shared_distance_view)
    lateinit var shared_distanceView: TextView
    @BindView(R.id.price_view)
    lateinit var priceView: TextView


    @BindView(R.id.cancel_button)
    lateinit var cancelButton: Button
    @BindView(R.id.request_button)
    lateinit var requestButton: Button
    @BindView(R.id.reject_button)
    lateinit var rejectButton: Button
    @BindView(R.id.accept_button)
    lateinit var acceptButton: Button



    private lateinit var liftMapFragment: LiftMapFragment
    private lateinit var way_id : String
    private lateinit var lift_id : String
    private lateinit var way: Way

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lift_detail)
        ButterKnife.bind(this)
        way_id = intent.getStringExtra(PARAMETER_WAY_ID)
        lift_id = intent.getStringExtra(PARAMETER_LIFT_ID)
        initMapFragment()
    }

    protected fun initMapFragment() {
        // Mapbox.getInstance(this.applicationContext, getString(R.string.mapbox_access_token))  // FIXME brauch ich das nochmal, wenns in der start-activity schon ist?
        liftMapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as LiftMapFragment
        liftMapFragment.arguments = intent.extras

    }

    override fun onHitchServiceAttach(api: Api) {
        way = api.ways.getWay(way_id)
        hitchServiceConnection.observe(api.ways.liftInfoStream(way_id,lift_id), this::processLiftInfo)
    }

    fun processLiftInfo(liftInfo:LiftInfo) {
        headerView.text = liftTitle(headerView.context, liftInfo.lift.status, liftInfo.role)

        nameView.text = liftInfo.partner.nick_name

        val time = liftInfo.lift.pickup_time
        pickup_timeView.text = Util.formatDateTime(pickup_timeView.context, Date(time))
        destinationPointView.text = liftInfo.lift.drop_off_point.toString()
        // detour_distanceView.text =  String.format("%.1f km", liftInfo.lift.detour_distance / 1000)  // fixme localize
        // val detour_time = liftInfo.lift.detour_time
        // detour_timeView.text = String.format("%d minutes", detour_time / 60)
        shared_distanceView.text =  String.format("%.1f km", liftInfo.lift.shared_distance / 1000)  // fixme localize
        val price = liftInfo.lift.price
        val currency = liftInfo.lift.currency
        priceView.text = Util.formatMoney(price, currency)
        setWarningPartial(liftInfo.lift)
        updateButtonVisibilities(liftInfo)
    }

    private fun setWarningPartial(lift: Lift) {
        val showWarning = way.role==Role.PASSENGER && lift.sectional
        if (showWarning) {
            partialLiftWarningView.visibility = LinearLayout.VISIBLE
            partialLiftWarningView.text = "⚠️"+getString(R.string.warning_partial)
        } else {
            partialLiftWarningView.visibility = LinearLayout.GONE
        }
    }


    public override fun onStart() {
        super.onStart()
    }

    @OnClick(R.id.cancel_button)
    fun onCancelButtonClick() {
        hitchServiceConnection.whenReady { api ->
            api.ways.cancelLift(way_id,lift_id)
            finish()
        }
    }


    @OnClick(R.id.request_button)
    fun onRequestButtonClick() {
        hitchServiceConnection.whenReady { api ->
            api.changeLiftStatus(way_id,lift_id, Lift.Status.REQUESTED)
            finish()
        }
    }

    @OnClick(R.id.reject_button)
    fun onRejectButtonClick() {
        hitchServiceConnection.whenReady { api ->
            api.changeLiftStatus(way_id,lift_id, Lift.Status.REJECTED)
            finish()
        }
    }

    @OnClick(R.id.accept_button)
    fun onAcceptButtonClick() {
        hitchServiceConnection.whenReady { api ->
            api.changeLiftStatus(way_id,lift_id, Lift.Status.ACCEPTED)
            finish()
        }
    }

    private fun updateButtonVisibilities(liftInfo:LiftInfo) {
        val role = liftInfo.role
        val status = liftInfo.lift.status
        cancelButton.visibility = visibility( !(status==Lift.Status.SUGGESTED || (role==Role.DRIVER && status==Lift.Status.REQUESTED)) )
        requestButton.visibility = visibility( role==Role.PASSENGER && status==Lift.Status.SUGGESTED)
        rejectButton.visibility = visibility(role==Role.DRIVER && status==Lift.Status.REQUESTED)
        acceptButton.visibility = visibility(role==Role.DRIVER && status==Lift.Status.REQUESTED)
    }

    private fun visibility(isVisible:Boolean) : Int = if(isVisible) View.VISIBLE else View.GONE

    companion object {
        val PARAMETER_WAY_ID = "way_id"
        val PARAMETER_LIFT_ID = "lift_id"
    }
}
