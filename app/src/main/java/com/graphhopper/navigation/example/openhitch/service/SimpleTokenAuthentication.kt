package com.graphhopper.navigation.example.openhitch.service

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.fasterxml.jackson.core.type.TypeReference
import com.graphhopper.navigation.example.BuildConfig
import com.graphhopper.navigation.example.openhitch.service.api.Authentication
import com.graphhopper.navigation.example.openhitch.service.api.AuthenticationState
import de.wifaz.oh.protocol.HitchInterface
import de.wifaz.oh.protocol.User
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.graphhopper.navigation.example.openhitch.Util
import com.graphhopper.navigation.example.openhitch.service.api.ApiException
import de.wifaz.oh.protocol.CreateUserResponse
import timber.log.Timber

class SimpleTokenAuthentication(private val context: Context, private val hitchInterface: HitchInterface) : Authentication {
    override var user: User? = null
        private set

    override var token: String? = null
        private set

    private val stateSubject : BehaviorSubject<AuthenticationState> = BehaviorSubject.create()

    override fun stateStream(): Observable<AuthenticationState> {
        return stateSubject
    }

    init {
        user = Util.load<User>(context,"oh.auth.user",object: TypeReference<User>(){})
        token = Util.load(context,"oh.auth.token",object: TypeReference<String>(){})

        if (token!=null) {
            stateSubject.onNext(AuthenticationState.OPEN)
        } else {
            stateSubject.onNext(AuthenticationState.CLOSED)
        }
    }

    override fun close() {
        stateSubject.onNext(AuthenticationState.CLOSED)
    }

    override fun register(newUser: User) {
        val call = hitchInterface.createUser(newUser);
        Timber.d("Calling register, user=%s",user)
        call.enqueue(RegisterCallback(newUser))
    }

    override fun updateUser(newUser: User) {
        Timber.d("Calling updateUser, user=%s",newUser)
        val call = hitchInterface.updateUser(newUser.id,newUser);
        call.enqueue(UpdateCallback(newUser))

    }

    override fun updateUserData(newUserData:User) {
        val oldUser = user
        if (oldUser==null) {
            throw ApiException("Internal error: fetched user data for unknown user")
        }
        if (newUserData.id!=oldUser.id) {
            throw ApiException("Internal error: received user data for wrong user")
        }
        Util.save(context,"oh.auth.user",user)
    }

    override fun setUser(user: User?, token: String?) {
        stateSubject.onNext(AuthenticationState.CLOSED)
        this.user = user
        this.token = token
        Util.save(context,"oh.auth.user",user)
        Util.save(context,"oh.auth.token",token)
        if(token!=null) {
            stateSubject.onNext(AuthenticationState.OPEN)
        }
    }

    inner class RegisterCallback(val newUser:User) : Callback<CreateUserResponse> {
        override  fun onResponse(call: Call<CreateUserResponse>, response: Response<CreateUserResponse>) {
            token = null
            if (response.isSuccessful) {
                token = response.body()?.token
            }
            if (token!=null) {
                user = newUser
                Util.save(context,"oh.auth.user",newUser)
                Util.save(context,"oh.auth.token",token)
                Timber.d("registering call successful, token=%s",token)
                stateSubject.onNext(AuthenticationState.REGISTRATION_SUCCESS)
                stateSubject.onNext(AuthenticationState.OPEN)
            } else {
                user = null
                Timber.i("registering call failed, status: %d, message: %s", response.code(), response.message())
                stateSubject.onNext(AuthenticationState.REGISTRATION_FAIL)
                stateSubject.onNext(AuthenticationState.CLOSED)
            }
        }
        override fun onFailure(call: Call<CreateUserResponse>, t: Throwable) {
            Timber.e(t, "Registering call failed (%s)", t.message)
            stateSubject.onNext(AuthenticationState.REGISTRATION_FAIL)
        }
    }

    inner class UpdateCallback(val newUser: User) : Callback<Void> {
        override  fun onResponse(call: Call<Void>, response: Response<Void>) {
            if (response.isSuccessful) {
                user = newUser
                Util.save(context,"oh.auth.user",newUser)
            } else {
                user = null
                Timber.i("registering call failed, status: %d, message: %s", response.code(), response.message())
            }
        }
        override fun onFailure(call: Call<Void>, t: Throwable) {
            Timber.e(t, "Registering call failed (%s)", t.message)
        }
    }


}