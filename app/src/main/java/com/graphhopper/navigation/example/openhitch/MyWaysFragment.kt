package com.graphhopper.navigation.example.openhitch

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.service.api.Api

import de.wifaz.oh.protocol.Role.DRIVER
import de.wifaz.oh.protocol.Role.PASSENGER
import de.wifaz.oh.protocol.Way

class MyWaysFragment : ListFragment<Way>() {

    override fun onItemClick(item: Way) {
        var intent = Intent(activity,WayDetailActivity::class.java)
        intent.putExtra(WayDetailActivity.PARAMETER_WAY_ID,item.id)
        startActivity(intent)
        activity!!.finish()
        Intent()
    }

    private fun updateItems(wayContexts: List<Way>) {
        // FIXME order?
        replaceItems(wayContexts)
    }

    override fun createView(container: ViewGroup): View {
        return activity!!.layoutInflater.inflate(R.layout.item_way, container, false)
    }

    override fun onHitchServiceAttach(api: Api) {
        hitchServiceConnection.observe(api.ways.wayListStream {true}, this::updateItems)
    }

    override fun initItemView(view: View, item: Way) {
        renderItemHeader(view, item)

        (view.findViewById<View>(R.id.way_view) as WayView).setWay(item)

        /* FIXME
        hitchServiceConnection.whenReady {
            when (item.way.role) {
                DRIVER -> {

                }
                PASSENGER -> partnerInfo = getOffersInfo(item)
            }
            hitchServiceConnection.observe(item.streamLiftInfoCount(states),)
        }
         */
    }

    /* FIXME
    private fun renderItemPartnerInfo(view: View, item: WayInfo) {
        val partnerInfo: String?
        when (item.way.role) {
            DRIVER -> partnerInfo = getPassengersInfo(item)
            PASSENGER -> partnerInfo = getOffersInfo(item)
            else -> throw RuntimeException("Incomplete switch")
        }
        val partnerInfoView = view.findViewById<TextView>(R.id.partner_info)
        partnerInfoView.text = partnerInfo
    }

    private fun getPassengersInfo(way: Way): String {
        if (way.role != DRIVER) {
            Timber.e("Internal error, wrong Role, not driver ")
            return "error"
        }
        val way_id = way.id
        if (!hitchService!!.isLoaded(way_id)) {
            return "..."
        }
        var result = ""
        val nAccepts = hitchService.getLiftInfos(way.id, EnumSet.of<Lift.Status>(Lift.Status.ACCEPTED, Lift.Status.STARTED)).size
        if (nAccepts > 0) {
            result += String.format(getString(R.string.passengerCount), nAccepts)
        }
        val nRequests = hitchService.getLiftInfos(way.id, EnumSet.of<Lift.Status>(Lift.Status.REQUESTED)).size
        if (nRequests > 0) {
            if (result.length > 0) {
                result += ", "
            }
            result += String.format(getString(R.string.passengerCount), nRequests)
        }
        return result
    }

    private fun getOffersInfo(way: Way): String? {
        val hitchService = hitchService!!
        if (way.role != PASSENGER) {
            Timber.e("Internal error, wrong Role, not passenger")
            return "error"
        }
        val way_id = way.id
        if (!hitchService.isLoaded(way_id)) {
            return "..."
        }
        val lift_infos = hitchService.getLiftInfos(way_id)
        val status: String? = null
        var nOffers = 0
        var nRequests = 0

        for (liftInfo in lift_infos) {
            when (liftInfo.status) {
                Lift.Status.SUGGESTED -> nOffers++
                Lift.Status.REQUESTED -> nRequests++
                Lift.Status.ACCEPTED -> return getString(R.string.ride_accepted)
                Lift.Status.REJECTED -> return getString(R.string.ride_rejected)
                Lift.Status.STARTED -> return getString(R.string.ride_started)
                Lift.Status.FINISHED -> return getString(R.string.ride_finished)
                Lift.Status.DRIVER_CANCELED -> return getString(R.string.ride_canceled_by_driver)
                Lift.Status.PASSENGER_CANCELED -> return getString(R.string.ride_canceled_by_you)
            }
        }
        if (status != null) {
            return status
        }
        if (nRequests > 0) {
            return String.format(getString(R.string.request_count), nRequests)
        }
        return if (nOffers > 0) {
            String.format(getString(R.string.offer_count), nOffers)
        } else ""
    }
     */

    private fun renderItemHeader(view: View, item: Way) {
        val headerResource: Int
        when (item.role) {
            DRIVER -> headerResource = R.string.drive
            PASSENGER -> headerResource = R.string.hitchhike
            else -> throw RuntimeException("Incomplete switch")
        }
        val headerView = view.findViewById<TextView>(R.id.header)
        headerView.setText(headerResource)
    }
}
