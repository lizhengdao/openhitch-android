package com.graphhopper.navigation.example.openhitch

import android.app.Activity
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.graphhopper.navigation.example.openhitch.service.HitchServiceConnection
import com.graphhopper.navigation.example.openhitch.service.api.Api

open class HitchFragment : Fragment(), HitchServiceConnection.Client {
    protected var hitchServiceConnection = HitchServiceConnection(this)

    @CallSuper
    override fun onStart() {
        super.onStart()
        hitchServiceConnection.open(context!!)
    }

    @CallSuper
    override fun onStop() {
        hitchServiceConnection.close()
        super.onStop()
    }

    override fun onHitchServiceAttach(api: Api) {
        // by default do nothing
    }
}
