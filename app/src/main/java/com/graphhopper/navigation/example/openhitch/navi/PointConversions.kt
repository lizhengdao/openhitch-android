package com.graphhopper.navigation.example.openhitch.navi

import android.location.Location
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import de.wifaz.oh.protocol.OHPoint
import java.util.ArrayList

fun Point.toOHPoint(): OHPoint {
    return OHPoint.fromLngLat(longitude(), latitude())
}

fun OHPoint.toPoint(): Point {
    return Point.fromLngLat(longitude, latitude)
}

fun LatLng.toString(): String {
    return latitude.toString() + "," + longitude.toString()
}

fun LatLng.toPoint(): Point {
    return Point.fromLngLat(longitude, latitude)
}

fun Location.toPoint(): Point {
    return Point.fromLngLat(longitude, latitude)
}

fun Location.toOHPoint(): OHPoint {
    return OHPoint.fromLngLat(longitude, latitude)
}
