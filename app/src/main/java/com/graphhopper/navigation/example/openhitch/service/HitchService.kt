package com.graphhopper.navigation.example.openhitch.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper

import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.service.api.Authentication
import com.graphhopper.navigation.example.openhitch.service.api.Api
import de.wifaz.oh.protocol.HitchInterface
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import timber.log.Timber
import java.io.IOException

// Service is autostarted on boot as described here: https://stackoverflow.com/questions/7690350/android-start-service-on-boot
// (currently not: removed Autostart from AndroidManifest.xml)

class HitchService : Service() {

    lateinit var api: Api private set
    lateinit var authentication: Authentication private set

    // cf https://developer.android.com/guide/components/bound-services.html#Binder
    // Binder given to clients
    private val binder = LocalBinder()

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        val service: HitchService
            get() = this@HitchService
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onCreate() {
        createNotificationChannel()
        val hitchInterface = createRetrofitClient()
        authentication = SimpleTokenAuthentication(this, hitchInterface)
        val mqttDownload = MqttDownload(this, authentication)
        val messageStream = mqttDownload.messageStream()
        api = Api(authentication, messageStream, hitchInterface)
        api.loadSyncDataFromServer()
    }

    override fun onStartCommand(intent: Intent?,
                                flags: Int,
                                startId: Int): Int {
        Timber.i( "entered onStartCommand")
        val notification = createNotification()
        startForeground(HITCH_SERVICE_NOTIFICATION_ID, notification)
        Timber.e( "started")

        return Service.START_STICKY
    }


    override fun onDestroy() {
        authentication.close()
        Timber.i( "HitchService stopped")
    }

    private fun createNotification(): Notification {
        val icon = BitmapFactory.decodeResource(resources,
                R.drawable.ic_thumb_up_black_24dp)

        return NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                // FIXME use resource strings
                .setContentTitle(getString(R.string.hitchservice_started))
                .setContentText(getString(R.string.hitchservice_description))

                // FIXME icon was null on boot
                // .setSmallIcon(R.drawable.ic_thumb_up_black_24dp)
                // .setLargeIcon(
                //        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setOngoing(true)
                .build()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }

        val notificationManager = getSystemService<NotificationManager>(NotificationManager::class.java)
                ?: throw HitchServiceInternalException("Could not create NotificationManager")

        val generalChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "OpenHitch", NotificationManager.IMPORTANCE_DEFAULT)
        generalChannel.description = "OpenHitch Meldungen"  // FIXME resource string
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        notificationManager.createNotificationChannel(generalChannel)
    }

    private fun createRetrofitClient() : HitchInterface {
        var hitchApiUrl:String = getString(R.string.base_url) + "hitch/"
        val objectMapper = ObjectMapper()
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        var httpClient = OkHttpClient.Builder()
                .addInterceptor(AuthenticationInterceptor())
                .build()
        var retrofit = Retrofit.Builder()
                .baseUrl(hitchApiUrl)
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .client(httpClient)
                .build()
        return retrofit.create<HitchInterface>(HitchInterface::class.java)
    }

    internal inner class AuthenticationInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            val original = chain.request()
            val request = original.newBuilder()
                    .addHeader("Authorization", "Bearer " + authentication.token)
                    .build()
            return chain.proceed(request)
        }
    }


    companion object {
        private val HITCH_SERVICE_NOTIFICATION_ID = 1
        val NOTIFICATION_CHANNEL_ID = "OpenHitch"
    }
}

// FIXME should be non-Runtime-Exceptions and properly caught
class HitchServiceInternalException(message: String) : java.lang.RuntimeException(message) {
}

class HitchProtocolException(message: String) : java.lang.RuntimeException(message) {
}
