package com.graphhopper.navigation.example.openhitch

import android.os.Bundle

import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.service.api.Api

class MyWaysActivity : HitchActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_ways)
    }

}
