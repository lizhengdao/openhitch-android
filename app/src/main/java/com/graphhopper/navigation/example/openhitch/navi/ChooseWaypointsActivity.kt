package com.graphhopper.navigation.example.openhitch.navi

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import com.graphhopper.navigation.example.NavigationViewSettingsActivity
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.HitchActivity
import com.graphhopper.navigation.example.openhitch.OfferActivity
import com.graphhopper.navigation.example.openhitch.SearchRideActivity
import com.graphhopper.navigation.example.openhitch.navi.MapFragment.Companion.DRIVER_ROUTE_SOURCE_ID
import com.graphhopper.navigation.example.openhitch.navi.MapFragment.Companion.PASSENGER_ROUTE_SOURCE_ID
import com.graphhopper.navigation.example.openhitch.service.HitchService
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.module.telemetry.TelemetryImpl
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.ui.v5.route.OnRouteSelectionChangeListener
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Role.DRIVER
import de.wifaz.oh.protocol.Role.PASSENGER
import timber.log.Timber

class ChooseWaypointsActivity :
        HitchActivity(), MapboxMap.OnMapClickListener, MapboxMap.OnMapLongClickListener,
        OnRouteSelectionChangeListener, OnMapReadyCallback {

    private final val LOCATION_PERMISSION_REQUEST_CODE: Int = 1

    private lateinit var role: Role
    private lateinit var mapRoute: NavigationMapRoute

    @BindView(R.id.all)
    lateinit var view: View
    @BindView(R.id.header)
    lateinit var header: TextView
    @BindView(R.id.launch_button_row)
    lateinit var launchButtonRow: View
    @BindView(R.id.proceed_button)
    lateinit var proceedButton: View

    internal lateinit var mapFragment: MapFragment
    private var route: DirectionsRoute? = null
    private lateinit var searchBox: SearchBox

    val mapboxMap: MapboxMap
        get() = mapFragment.mapboxMap

    val mapView: MapView
        get() = mapFragment.mapView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waypoints)
        Mapbox.getInstance(this.applicationContext, getString(R.string.mapbox_access_token))
        TelemetryImpl.disableOnUserRequest()
        ButterKnife.bind(this)
        getParameters()

        val mainToolbar = findViewById<Toolbar>(R.id.main_toolbar)
        setSupportActionBar(mainToolbar)

        supportActionBar?.setTitle("")
        showFirstStartIfNecessary()

        setHeader()

        mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as MapFragment
        mapFragment.setOnMapReadyCallback(this)

        startService(Intent(this, HitchService::class.java))
    }

    override fun onStart() {
        super.onStart()
        checkLocationPermission()
    }

    private fun checkLocationPermission() {
        // FIXME give user a chance to deny permanently without being begged again
        val permission = Manifest.permission.ACCESS_FINE_LOCATION
        val locationAccess = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
        if (!locationAccess) {
            ActivityCompat.requestPermissions(this, arrayOf(permission), LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private fun getParameters() {
        role = intent.getSerializableExtra(PARAMETER_ROLE) as Role
    }

    private fun setHeader() {
        when (role) {
            DRIVER -> header.text = getString(R.string.offer_ride)
            PASSENGER -> header.text = getString(R.string.search_ride)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.navigation_view_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settings -> {
                showSettings()
                return true
            }
            R.id.help -> {
                showHelpDialog()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showSettings() {
        startActivityForResult(Intent(this, NavigationViewSettingsActivity::class.java), CHANGE_SETTING_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CHANGE_SETTING_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val shouldRefetch = (data!!.getBooleanExtra(NavigationViewSettingsActivity.UNIT_TYPE_CHANGED, false)
                    || data.getBooleanExtra(NavigationViewSettingsActivity.LANGUAGE_CHANGED, false)
                    || data.getBooleanExtra(NavigationViewSettingsActivity.PROFILE_CHANGED, false))
            // FIXME was macht graphhopper example wenn shouldRefetch==false
            searchBox.updateRoute()
        }
    }

    @OnClick(R.id.cancel_button)
    fun cancelButtonClick() {
        finish()
    }

    @OnClick(R.id.proceed_button)
    fun okButtonClick() {
        val waypoints = searchBox.waypoints
        val route = this.route
        if (waypoints == null || route==null) {
            // Button should be disabled, when the waypoints or route are not valid
            Timber.e("Internal error, ok-button click without valid waypoints or route")
            return
        }
        val duration = (route.duration()!!*1000.0).toLong()
        when (role) {
            DRIVER -> {
                val intent = Intent(this, OfferActivity::class.java)
                intent.putExtra(OfferActivity.PARAMETER_WAYPOINTS, waypoints)
                intent.putExtra(OfferActivity.PARAMETER_DURATION, duration)
                finish()
                startActivity(intent)
            }
            PASSENGER -> {
                val intent = Intent(this, SearchRideActivity::class.java)
                intent.putExtra(SearchRideActivity.PARAMETER_WAYPOINTS, waypoints)
                intent.putExtra(SearchRideActivity.PARAMETER_DURATION, duration)
                finish()
                startActivity(intent)
            }
        }
    }


    public override fun onResume() {
        super.onResume()
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        // getIntent() should always return the most recent
        setIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        /*
        if (intent != null) {
            Uri data = intent.getData();
            if (data != null && "graphhopper.com".equals(data.getHost())) {
                if (data.getPath() != null) {
                    if (this.mapboxMap == null) {
                        //this happens when onResume is called at the initial start and we will call this method again in onMapReady
                        return;
                    }
                    if (data.getPath().contains("maps")) {
                        clearRoute();
                        //Open Map Url
                        setRouteProfileToSharedPreferences(data.getQueryParameter("vehicle"));

                        List<Point> waypoints = new ArrayList<>();
                        List<String> points = data.getQueryParameters("ohPoint");
                        for (String ohPoint : points) {
                            String[] pointArr = ohPoint.split(",");
                            double lng = Double.parseDouble(pointArr[0]);
                            double lat = Double.parseDouble(pointArr[1]);
                            waypoints.add(Point.fromLngLat(lng, lat));
                            searchBox.setWaypoints(waypoints);
                        }
                    }
                }

            }
        }
         */
    }

    private fun showFirstStartIfNecessary() {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        if (sharedPreferences.getInt(getString(R.string.first_start_dialog_key), -1) < FIRST_START_DIALOG_VERSION) {
            showHelpDialog()
        }
    }

    private fun showHelpDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.help_message_title)
        builder.setMessage(Html.fromHtml("1. Please note, this is a demo and not a full featured application. The purpose of this application is to provide an easy starting ohPoint for developers to create a navigation application with GraphHopper<br/>2. You should enable your GPS/location<br/>3.You can either search for a location using the magnifier icon or by long pressing on the map<br/>4. Start the navigation by tapping the arrow button<br/><br/>This project is 100% open source, contributions are welcome.<br/><br/>Please drive carefully and always abide local law and signs. Roads might be impassible due to construction projects, traffic, weather, or other events."))
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        builder.setPositiveButton(R.string.agree) { _, _ ->
            val editor = sharedPreferences.edit()
            editor.putInt(getString(R.string.first_start_dialog_key), FIRST_START_DIALOG_VERSION)
            editor.apply()
        }
        builder.setNeutralButton(R.string.github) { _, _ -> startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/graphhopper/graphhopper-navigation-example"))) }

        val dialog = builder.create()
        dialog.show()
    }

    override fun onDestroy() {
        searchBox.exit()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("route", route)
        // FIXME save and restore way points
        // FIXME test this
        // FIXME more structure (something like XyzActivity.State extends Serializable)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        route = savedInstanceState?.getSerializable("route") as DirectionsRoute?
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        mapboxMap.addOnMapClickListener(this)
        mapboxMap.addOnMapLongClickListener(this)
        initMapRoute()

        handleIntent(intent)

        searchBox = SearchBox()
        searchBox.init(this)
    }

    override fun onMapClick(point: LatLng) {
        onAnyMapClick(point)
    }

    override fun onMapLongClick(point: LatLng) {
        onAnyMapClick(point)
    }

    private fun onAnyMapClick(point: LatLng) {
        if (!isKeyboardShowing) {
            searchBox.setPoint(point.toPoint())
            searchBox.updateRoute()
        }
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mapView.windowToken, 0)
        isKeyboardShowing = false
    }

    override fun onNewPrimaryRouteSelected(directionsRoute: DirectionsRoute) {
        route = directionsRoute
    }

    private fun initMapRoute() {
        mapRoute = NavigationMapRoute(mapView, mapboxMap)
        mapRoute.setOnRouteSelectionChangeListener(this)
    }

    fun updateRoute(waypoints: List<Point>?) {
        if (waypoints != null) {
            fetchRoute(this,waypoints, null, hitchServiceConnection.Callback<DirectionsRoute>(this::onRouteResponse, this::onRouteFailure))
        } else {
            mapboxMap.clear()
            proceedButton.isEnabled = false
        }
        hideKeyboard()
    }

    fun onRouteResponse(route: DirectionsRoute) {
        this.route = route
        proceedButton.isEnabled = true
        var sourceId:String = when(role) {
            DRIVER -> DRIVER_ROUTE_SOURCE_ID
            PASSENGER -> PASSENGER_ROUTE_SOURCE_ID
        }
        mapFragment.drawRoute(route,sourceId)
    }

    private fun onRouteFailure(throwable: Throwable) {
        Snackbar.make(mapView, getString(R.string.error_calculating_route)+": "+throwable.message, Snackbar.LENGTH_LONG).show()
    }

    companion object {

        val PARAMETER_ROLE = "role"
        private val FIRST_START_DIALOG_VERSION = 1

        val CAMERA_ANIMATION_DURATION = 1000
        private val CHANGE_SETTING_REQUEST_CODE = 1
    }

    // Messy way of bookkeeping if the soft keyboard is open
    // There doesn't seems to be a way to determine this, see also comments to
    // Rich Schuler's answer on https://stackoverflow.com/questions/3081276/android-detect-softkeyboard-open
    // Note that I made this.activity public only to make isKeyboardShowing accessible to SearchLocationFragment
    public var isKeyboardShowing:Boolean = false;
}
