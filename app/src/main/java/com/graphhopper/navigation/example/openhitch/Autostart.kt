package com.graphhopper.navigation.example.openhitch

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.graphhopper.navigation.example.openhitch.service.HitchService
import timber.log.Timber

class Autostart : BroadcastReceiver() {
    override fun onReceive(context: Context, arg1: Intent) {
        val intent = Intent(context, HitchService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        } else {
            context.startService(intent)
        }
        Timber.i("Autostart: started")
    }
}
