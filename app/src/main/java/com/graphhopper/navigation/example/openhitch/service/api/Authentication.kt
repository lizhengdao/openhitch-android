package com.graphhopper.navigation.example.openhitch.service.api

import de.wifaz.oh.protocol.User
import io.reactivex.Observable

enum class AuthenticationState {
    REGISTER,
    REGISTRATION_SUCCESS,
    REGISTRATION_FAIL,
    LOGIN,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    OPEN,
    CLOSED
}

/**
 * XXX null means not registered
 */
interface Authentication {
    val user : User?
    fun register(user: User)
    fun updateUser(user: User)

    @ApiInternal
    // FIXME cannot declare this internal
    fun updateUserData(newUserData: User)

    /**
     * Forget all user and authentication info.
     *
     * This does not delete any information from the server. For debugging purposes only
     */
    fun setUser(user:User?,token:String?)

    /// Later
    // fun login(credentials:Credentials) : Boolean
    // fun logout()

    fun stateStream() : Observable<AuthenticationState>
    fun close()

    // FIXME server side only, don't leak this into GUI
    val token:String?
}