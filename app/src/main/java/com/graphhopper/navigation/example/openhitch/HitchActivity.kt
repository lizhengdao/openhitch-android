package com.graphhopper.navigation.example.openhitch

import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import com.graphhopper.navigation.example.openhitch.service.HitchServiceConnection
import com.graphhopper.navigation.example.openhitch.service.api.Api

open class HitchActivity : AppCompatActivity(), HitchServiceConnection.Client {
    var hitchServiceConnection = HitchServiceConnection(this)

    @CallSuper
    override fun onStart() {
        super.onStart()
        hitchServiceConnection.open(this)
    }

    @CallSuper
    override fun onStop() {
        hitchServiceConnection.close()
        super.onStop()
    }

    override fun onHitchServiceAttach(api: Api) {
        // by default do nothing
    }
}
