package com.graphhopper.navigation.example.openhitch.navi

import android.os.Bundle
import androidx.annotation.CallSuper
import com.google.android.material.snackbar.Snackbar
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.WayDetailActivity
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.mapboxsdk.maps.MapboxMap
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Role
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WayMapFragment : MapFragment() {
    private lateinit var wayId: String

    private fun showRoutes(waypoints: List<OHPoint>) {
        showLoading()
        fetchRoute(context!!, waypoints.map {it.toPoint()}, lastKnownLocation, hitchServiceConnection.Callback<DirectionsRoute>( this::onRouteResponse, this::onRouteFailure))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        wayId = arguments!!.getString(WayDetailActivity.PARAMETER_WAY_ID)!!
    }

    @CallSuper
    override fun onMapReady(mapboxMap: MapboxMap) {
        super.onMapReady(mapboxMap)

        hitchServiceConnection.whenReady { api ->
            val way = api.ways.getWay(wayId)
            if (way.role==Role.DRIVER) {
                val observable = api.ways.waypointsStream(wayId)
                observable.observeOn(Schedulers.io())
                        .subscribeOn(AndroidSchedulers.mainThread())
                hitchServiceConnection.observe(observable, this::showRoutes)
            } else {
                showLoading()
                fetchRoute(context!!, way.waypoints.map {it.toPoint()}, lastKnownLocation, hitchServiceConnection.Callback<DirectionsRoute>( this::onRouteResponse, this::onRouteFailure))
            }
        }
    }

    private fun onRouteResponse(route: DirectionsRoute) {
        drawRoute(route, MapFragment.DRIVER_ROUTE_SOURCE_ID)
        hideLoading()
    }

    private fun onRouteFailure(throwable: Throwable) {
        Snackbar.make(mapView, getString(R.string.error_calculating_route)+": "+throwable.message, Snackbar.LENGTH_LONG).show()
        hideLoading()
    }
}