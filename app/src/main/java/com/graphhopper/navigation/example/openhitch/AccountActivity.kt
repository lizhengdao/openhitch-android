package com.graphhopper.navigation.example.openhitch

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import com.graphhopper.navigation.example.BuildConfig
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.service.api.Api
import com.graphhopper.navigation.example.openhitch.service.api.AuthenticationState
import de.wifaz.oh.protocol.User

class AccountActivity : HitchActivity() {
    companion object {
        final val REQUEST_REGISTER: Int = 1
    }

    @BindView(R.id.account_header)
    lateinit var accountHeader: TextView
    @BindView(R.id.account_info)
    lateinit var accountInfo: TextView
    @BindView(R.id.submit_button)
    lateinit var submitButton: Button
    @BindView(R.id.cancel_button)
    lateinit var cancelButton: Button

    @BindView(R.id.edit_first_name)
    lateinit var editFirstName: EditText
    @BindView(R.id.edit_last_name)
    lateinit var editLastName: EditText
    @BindView(R.id.edit_phone_number)
    lateinit var editPhoneNumber: EditText
    @BindView(R.id.status_display)
    lateinit var statusDisplay: TextView

    @BindView(R.id.developer_options)
    lateinit var developerOptions: View
    @BindView(R.id.persona_spinner)
    lateinit var personaSpinner: Spinner
    @BindView(R.id.user_id_display)
    lateinit var userIdDisplay: TextView
    @BindView(R.id.token_display)
    lateinit var tokenDisplay: TextView
    @BindView(R.id.delete_user_button)
    lateinit var deleteUserButton: Button

    @BindView(R.id.all)
    lateinit var rootView: View

    var currentUser: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        ButterKnife.bind(this)
    }

    override fun onHitchServiceAttach(api: Api) {
        super.onHitchServiceAttach(api)
        currentUser = api.authentication.user
        val user = currentUser
        setTexts(user)
        fillUserData(user)
        if (BuildConfig.DEBUG) {
            initDeveloperOptions(api)
        }
    }

    private fun setTexts(user: User?) {
        if (user == null) {
            accountHeader.text = getString(R.string.registration)
            accountInfo.text = getString(R.string.registration_needed)
            submitButton.text = getString(R.string.register)
        } else {
            accountHeader.text = getString(R.string.user_data)
            accountInfo.visibility = GONE
            submitButton.text = getString(R.string.apply)
        }
    }

    private fun fillUserData(user: User?) {
        editFirstName.setText( user?.first_name ?: "")
        editLastName.setText( user?.last_name ?: "")
        editPhoneNumber.setText( user?.phone ?: "")
    }

    fun getNonEmptyValue(editText:EditText) : String {
        val value = editText.text.toString()
        if (value=="") {
            editText.setError(getString(R.string.field_mandatory))
            throw EmptyFieldException()
        }
        return value
    }

    @OnClick(R.id.submit_button)
    fun onSubmitButtonClick() {
        val user = currentUser
        try {
            val firstName = getNonEmptyValue(editFirstName)
            val lastName = getNonEmptyValue(editLastName)
            val phone = getNonEmptyValue(editPhoneNumber)
            val id = currentUser?.id ?: Util.randomId()
            val userData = User(
                    id = id,
                    first_name = firstName,
                    last_name = lastName,
                    nick_name = "$firstName $lastName",
                    phone = phone
            )

            if(user==null) {
                hitchServiceConnection.whenReady { api ->
                    api.authentication.stateStream().subscribe(this::onAuthenticationStateChange)
                    api.authentication.register(userData)
                }
            } else {
                hitchServiceConnection.whenReady { api ->
                    api.authentication.updateUserData(userData)
                }
            }
        } catch (e:EmptyFieldException) {
            // Do nothing, let registration for stay
        }
    }

    class EmptyFieldException : Exception() {}

    fun onAuthenticationStateChange(state:AuthenticationState) {
        when(state) {
            AuthenticationState.REGISTER -> {
                statusDisplay.text = getText(R.string.registration_running)
            }
            AuthenticationState.REGISTRATION_FAIL -> {
                statusDisplay.text = ""
                Snackbar.make(rootView,R.string.registration_failed,Snackbar.LENGTH_LONG).show()
            }
            AuthenticationState.OPEN -> {
                setResult(Activity.RESULT_OK)
                finish()
            }
            else -> {
                // ignore
            }
        }
    }

    @OnClick(R.id.cancel_button)
    fun onCancelButtonClick() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    private fun initDeveloperOptions(api: Api) {
        developerOptions.visibility = VISIBLE

        val personas : Array<User> = arrayOf(
                User(id="idPersona_Sabine", first_name = "Sabine", last_name = "Müller", phone = "0130 1234567"),
                User(id="idPersona_Max", first_name = "Max", last_name = "Schmidt", phone = "0130 2345678"),
                User(id="idPersona_Holger", first_name = "Holger", last_name = "Schneider", phone = "0130 3456789"),
                User(id="idPersona_Lisa", first_name = "Lisa", last_name = "Fischer", phone = "0130 4567890"),
                User(id="idPersona_Kerim", first_name = "Kerim", last_name = "Meyer", phone = "0130 5678901"),
                User(id="idPersona_Mia", first_name = "Mia", last_name = "Weber", phone = "0130 6789012")
        )
        val items = arrayOf("Choose...")+ personas.map {user -> user.first_name }

        fun refresh() {
            currentUser = api.authentication.user
            setTexts(currentUser)
            fillUserData(currentUser)
            userIdDisplay.text = currentUser?.id ?: ""
            tokenDisplay.text = api.authentication.token ?: ""
        }

        refresh()

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, items)
        personaSpinner.adapter = adapter
        personaSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // ignore
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position==0) {
                    return
                }
                val user = personas[position-1]
                val token = user.id.replace("idPersona_","tokenPersona_")
                api.authentication.setUser(user,token)
                api.loadSyncDataFromServer()
                finish()
            }

        })

        deleteUserButton.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                api.authentication.setUser(null,null)
                finish()
            }
        })

    }
}