package com.graphhopper.navigation.example.openhitch.service

import android.app.Service
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.ObjectMapper
import com.graphhopper.navigation.example.openhitch.service.api.Authentication
import com.graphhopper.navigation.example.openhitch.service.api.AuthenticationState
import de.wifaz.oh.protocol.*
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import timber.log.Timber

class MqttDownload (private val service: Service, private val authentication: Authentication): MqttCallback {
    private val mqttServerURI = "tcp://10.0.2.2:1883" // FIXME
    private var mqttAndroidClient : MqttAndroidClient? = null
    private val messageSubject: PublishSubject<HitchMessage> = PublishSubject.create()

    init {
        authentication.stateStream().subscribe(this::onAuthenticationStateChange)
    }

    fun messageStream() : Observable<HitchMessage> {
        return messageSubject;
    }

    private fun createMqttClient() : MqttAndroidClient {
        // FIXME use user id?
        val clientId = MqttClient.generateClientId()

        // FIXME configure IP
        // FIXME use MqttClientPersistence as a third parameter?
        val client = MqttAndroidClient(service.applicationContext, mqttServerURI, clientId)
        client.setCallback(this)
        return client
    }

    private fun mqttConnect(client:MqttAndroidClient, topic:String) {
        // For using the Eclipse Paho Android Service cf https://github.com/eclipse/paho.mqtt.android/blob/develop/BasicExample/src/main/java/paho/mqtt/java/example/PahoExampleActivity.java

        if (client.isConnected) {
            return
        }


        try {
            val mqttConnectOptions = MqttConnectOptions()
            mqttConnectOptions.isAutomaticReconnect = true
            mqttConnectOptions.isCleanSession = true
            client.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    val options = DisconnectedBufferOptions()
                    options.isBufferEnabled = true
                    options.bufferSize = 100
                    options.isPersistBuffer = true
                    options.isDeleteOldestMessages = false
                    client.setBufferOpts(options)
                    mqttSubscribe(client,topic)
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    // FIXME Fehlerstatus irgendwo sichtbar machen?
                    Timber.e( exception,"Failed to connect to Mqtt-Server %s", mqttServerURI)
                }
            })
        } catch (e: MqttException) {
            // FIXME Fehlerstatus irgendwo sichtbar machen?
            Timber.e( e,"Crashed on attempt to connect to Mqtt-Server %s", mqttServerURI)
        }

    }

    fun mqttSubscribe(client: MqttAndroidClient, topic: String) {
        try {
            client.subscribe(topic, 1, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    Timber.d( "Mqtt subscribed topic %s", topic)
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Timber.e(exception, "Mqtt subscription failed")
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribe on the specified topic e.g. using wildcards
                    // FIXME Fehlerstatus sichtbar machen
                }
            })
        } catch (e: MqttException) {
            Timber.e( e,"Crashed on attempt to subscribe Mqtt topic $topic")
            // FIXME Fehlerstatus sichtbar machen
        }
    }

    @Synchronized
    override fun messageArrived(topic: String, message: MqttMessage) {
        try {
            Timber.i( "MQTT message arrived, topic=%s, message=%s",topic,message)
            val json = message.toString()
            val mapper = ObjectMapper()
            // FIXME enable this in publish build variant
            // mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            val messageObject: HitchMessage
            try {
                messageObject = mapper.readValue<HitchMessage>(json, HitchMessage::class.java)
                messageSubject.onNext(messageObject)
            } catch (e: JsonParseException) {
                // FIXME report error to UI
                Timber.e( e,"MQTT message json error, message=%s", json)
                return
            }

        } catch (e: Exception) {
            Timber.e( e,"messageArrived crashed")
        }

    }

    override fun connectionLost(cause: Throwable) {
        // FIXME
    }

    override fun deliveryComplete(token: IMqttDeliveryToken) {
        // Nothing to do as we do not send messages
    }

    @Synchronized fun onAuthenticationStateChange(state:AuthenticationState) {
        when(state) {
            AuthenticationState.OPEN->{
                val client = createMqttClient()
                val topic = authentication.user!!.id
                Timber.d("Connecting to MQTT topic %s", topic)
                mqttConnect(client,topic)
                mqttAndroidClient = client
            }
            AuthenticationState.CLOSED->{
                mqttAndroidClient?.close()
                mqttAndroidClient = null
            }
            else -> {
                // ignore
            }
        }
    }
}