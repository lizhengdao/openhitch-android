package com.graphhopper.navigation.example.openhitch

import android.content.Context
import android.text.format.DateUtils
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.graphhopper.navigation.example.Constants
import com.graphhopper.navigation.example.R
import com.mapbox.services.android.navigation.v5.utils.LocaleUtils
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Role
import timber.log.Timber
import java.io.IOException
import java.lang.IllegalArgumentException
import java.lang.RuntimeException
import java.math.BigDecimal
import java.security.SecureRandom
import java.text.DateFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

object Util {
    private val base64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
    private val randomIdLength = 27 //

    internal var localeUtils = LocaleUtils()

    private val random = SecureRandom()

    /**
     * Create random id as a string
     *
     * @return random String of 27 base64 characters (=entropy 162 bit)
     */
    fun randomId(): String {
        val sb = StringBuilder(randomIdLength)
        for (i in 0 until randomIdLength)
            sb.append(base64chars[random.nextInt(base64chars.length)])
        return sb.toString()
    }

    fun isEmpty(s: String?): Boolean {
        return s == null || s.length == 0
    }

    fun formatDate(context: Context, date: Date): String {
        if (DateUtils.isToday(date.time)) {
            return context.getString(R.string.today)
        } else if (isTomorrow(date.time)) {
            return context.getString(R.string.tomorrow)
        }
        else {
            val df1 = SimpleDateFormat("EEEE ")  // day of week
            val df2 = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
            return df1.format(date) + df2.format(date)
        }
    }

    private fun isTomorrow(time: Long): Boolean {
        val then = GregorianCalendar()
        then.timeInMillis = time
        val tomorrow: Calendar = GregorianCalendar()
        tomorrow.add(Calendar.DAY_OF_MONTH,1)
        return then.get(Calendar.YEAR)==tomorrow.get(Calendar.YEAR) && then.get(Calendar.DAY_OF_YEAR)==tomorrow.get(Calendar.DAY_OF_YEAR)
    }

    fun truncateDate(date: Date): Date {
        val cal: Calendar = GregorianCalendar()
        cal.time = date
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal.time
    }

    fun getTimeOfDay(date: Date) : Int {
        return (date.time-truncateDate(date).time).toInt()
    }

    fun formatTime(context: Context, timeOfDay: Int) : String {
        val time = truncateDate(Date()).time+timeOfDay
        val tf = android.text.format.DateFormat.getTimeFormat(context)
        return tf.format(Date(time))
    }


    fun formatDateTime(context: Context, date: Date) : String {
        return formatDate(context,date)+" "+ formatTime(context, getTimeOfDay(date))
    }

    fun formatMoney(price: BigDecimal, currency: String): String {
        return NumberFormat.getCurrencyInstance().format(price);
        // FIXME: we would rather want the following, but it doesn't seem to work everywhere
        /*
        val monetaryAmount = Money.of(price, currency)
        val format = MonetaryFormats.getAmountFormat(Locale.getDefault())
        return format.format(monetaryAmount)
         */
    }

    fun <T> getLast(list: List<T>): T {
        return list[list.size - 1]
    }

    fun <T>load(context:Context,name:String,typeReference: TypeReference<T>) : T? {
        val preferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val json = preferences.getString(name, null)
        if (json==null) {
            return null
        }
        val mapper = ObjectMapper()
        try {
            var result:T = mapper.readValue<T>(json,typeReference);
            return result
            // val collectionType = mapper.typeFactory.constructCollectionType(List<*>::class.java!!, OHPoint::class.java!!)
            // favorites = mapper.readValue<List<OHPoint>>(json, collectionType)
        } catch (e: Exception) {
            when(e) {
                is IOException, is ClassCastException -> {
                    Timber.e(e,"loading $name: Could not convert stored json to java object, deleting unreadable stored value. json=%s", json)
                    return null
                }
                else -> throw e
            }
        }
    }

    fun <T>save(context: Context, name: String, value:T?) {
        val mapper = ObjectMapper()
        try {
            val json = mapper.writeValueAsString(value)
            val editor = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
            editor.putString(name, json)
            editor.commit()
        } catch (e: JsonProcessingException) {
            Timber.e(e,"Error on writing %s to json String", name)
        }

    }
}

public fun liftTitle (context: Context, status: Lift.Status, role: Role) : String {
    var resId : Int = when(status) {
        Lift.Status.SUGGESTED -> R.string.ride_offer
        Lift.Status.REQUESTED -> R.string.ride_request
        Lift.Status.ACCEPTED -> when(role) {
            Role.DRIVER -> R.string.passenger
            Role.PASSENGER -> R.string.agreed_lift
        }
        Lift.Status.STARTED -> R.string.started_ride
        Lift.Status.FINISHED -> R.string.finished_ride
        Lift.Status.DRIVER_CANCELED -> R.string.lift_title_driver_canceled
        Lift.Status.PASSENGER_CANCELED -> R.string.lift_title_passenger_canceled
        Lift.Status.REJECTED -> R.string.lift_title_rejected
    }
    return context.getText(resId) as String
}

