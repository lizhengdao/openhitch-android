package com.graphhopper.navigation.example.openhitch

import com.graphhopper.navigation.example.BuildConfig
import androidx.multidex.MultiDexApplication
import timber.log.Timber

class HitchApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}