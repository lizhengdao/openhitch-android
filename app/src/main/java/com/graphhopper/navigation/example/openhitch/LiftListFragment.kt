package com.graphhopper.navigation.example.openhitch

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.service.api.Api
import com.graphhopper.navigation.example.openhitch.service.reference_way_id

import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.util.*

class LiftListFragment : ListFragment<LiftInfo>(), AdapterView.OnItemClickListener {

    private lateinit var wayId: String
    private lateinit var way: Way

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        wayId = arguments!!.getString(WayDetailActivity.PARAMETER_WAY_ID)!!
    }

    override val emptyText: String?
        get() {
            val resourceId = when(way.role) {
                Role.PASSENGER -> R.string.empty_offer_list_text
                Role.DRIVER -> R.string.empty_passenger_list_text
            }
            return getString(resourceId)
        }


    override fun onHitchServiceAttach(api: Api) {
        way = api.ways.getWay(wayId)  // FIXME too late?
        val statusSet = when(way.role) {
            Role.DRIVER -> EnumSet.of(Lift.Status.REQUESTED, Lift.Status.ACCEPTED, Lift.Status.STARTED,Lift.Status.PASSENGER_CANCELED)
            Role.PASSENGER -> EnumSet.of(Lift.Status.SUGGESTED, Lift.Status.REQUESTED, Lift.Status.ACCEPTED, Lift.Status.STARTED,Lift.Status.DRIVER_CANCELED)
        }
        hitchServiceConnection.observe(api.ways.liftInfoListStream(wayId,statusSet),this::updateLifts)
    }

    override fun onItemClick(item: LiftInfo) {
        val intent = Intent(activity, LiftDetailActivity::class.java)
        intent.putExtra(LiftDetailActivity.PARAMETER_WAY_ID, item.reference_way_id())
        intent.putExtra(LiftDetailActivity.PARAMETER_LIFT_ID, item.lift.id)
        startActivity(intent)
    }

    public override fun createView(container: ViewGroup): View {
        return LiftInfoView(context!!)
    }

    override fun initItemView(view: View, item: LiftInfo) {
        (view as LiftInfoView).setLiftInfo(item,way)
    }

    private fun updateLifts(offers:List<LiftInfo>) {
        Collections.sort(offers, LiftComparator())
        replaceItems(offers)
    }

    private inner class LiftComparator : Comparator<LiftInfo> {
        // FIXME order for driver
        override fun compare(a: LiftInfo, b: LiftInfo): Int {
            return a.lift.rating.compareTo(b.lift.rating)
        }
    }

}
