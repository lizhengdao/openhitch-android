package com.graphhopper.navigation.example.openhitch.service

import android.util.Log
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import io.reactivex.subjects.BehaviorSubject

fun LiftInfo.reference_way_id() : String {
    return when(this.role) {
        Role.DRIVER -> this.lift.driver_way_id
        Role.PASSENGER -> this.lift.passenger_way_id
    }
}
