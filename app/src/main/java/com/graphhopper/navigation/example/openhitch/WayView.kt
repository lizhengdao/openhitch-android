package com.graphhopper.navigation.example.openhitch

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView

import com.graphhopper.navigation.example.R

import java.util.Date

import de.wifaz.oh.protocol.Way

class WayView @JvmOverloads constructor(private val aContext: Context, attrs: AttributeSet? = null) : RelativeLayout(aContext, attrs) {
    private val view: View
    var partnerInfo:TextView

    init {
        view = View.inflate(context, R.layout.view_way, this)
        partnerInfo = view.findViewById(R.id.partner_info)
    }

    fun setWay(way: Way) {
        renderWaypoints(way)
        renderStartTime(way)
        renderStatus(way)
    }

    private fun renderStatus(way: Way) {
        val statusView = view.findViewById<TextView>(R.id.status_view)
        val rid : Int = when(way.status) {
            Way.Status.PRELIMINARY -> R.string.way_status_preliminary  // should not be seen by user
            Way.Status.RESEARCH -> R.string.way_status_research
            Way.Status.PUBLISHED -> R.string.way_status_published
            Way.Status.STARTED -> R.string.way_status_started
            Way.Status.FINISHED -> R.string.way_status_finished
            Way.Status.CANCELED -> R.string.way_status_canceled
        }
        statusView.text = context.getString(rid)
    }

    private fun renderWaypoints(way: Way) {
        val waypoints = way.waypoints
        val origin = waypoints[0]
        val originPointView = view.findViewById<TextView>(R.id.origin_point_view)
        originPointView.text = origin.toString()
        val destination = Util.getLast(waypoints)
        val destinationPointView = view.findViewById<TextView>(R.id.destination_point_view)
        destinationPointView.text = destination.toString()
    }

    private fun renderStartTime(way: Way) {
        val startTimeView = view.findViewById<TextView>(R.id.start_time_view)
        val startTime = Date(way.start_time)
        startTimeView.text = Util.formatDateTime(context, startTime)
    }
}
