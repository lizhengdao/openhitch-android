package com.graphhopper.navigation.example.openhitch.service

import androidx.annotation.CallSuper
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.util.Collections.list
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * XXX android lists only update as a whole
 * XXX null values
 */
abstract class ListStore<X> {
    protected abstract fun getKey(x:X) : String;

    private val map : MutableMap<String,X> = HashMap()
    private val listSubject: PublishSubject<Map<String,X?>> = PublishSubject.create()
    private val entrySubject: PublishSubject<Pair<String,X?>> = PublishSubject.create()

    @Synchronized
    fun getItem(key: String) : X? {
        return map.get(key)
    }

    // FIXME testing only?
    @Synchronized
    fun getCurrentItems() : List<X> {
        return ArrayList(map.values)
    }

    @CallSuper
    protected open fun internalPut(key:String,x:X?) {
        entrySubject.onNext(Pair(key,x))
        if(x==null) {
            map.remove(key)
        } else {
            map.put(key,x)
        }
    }

    fun put(x:X) {
        val m = HashMap<String,X?>()
        m.put(getKey(x),x)
        putMap(m)
    }

    fun remove(key:String) {
        val m = HashMap<String,X?>()
        m.put(key,null)
        putMap(m)
    }


    fun putList(list:List<X>) {
        val m = HashMap<String,X?>()
        for (x in list) {
            m.put(getKey(x),x)
        }
        putMap(m)
    }

    fun erase() {
        val m = map.mapValues({null})
        putMap(m)
    }

    @Synchronized
    fun putMap(m: Map<String, X?>) {
        for ((key, x) in m) {
            internalPut(key,x)
        }
        listSubject.onNext(m)
    }

    private inner class ItemFilterOperator(private val observer:Observer<in X>,private val id:String) : Observer<Pair<String,X?>> {
        override fun onComplete() {
            observer.onComplete()
        }

        override fun onSubscribe(d: Disposable) {
            observer.onSubscribe(d)
            map.get(id)?.let {push(it)}
        }

        override fun onNext(entry: Pair<String, X?>) {
            if(id.equals(entry.first)) {
                entry.second?.let {push(it)}
            }
        }

        override fun onError(e: Throwable) {
            observer.onError(e)
        }

        private fun push(x:X) {
            observer.onNext(x)
        }
    }

    private inner class ItemObservable (private val id:String) : Observable<X>() {
        override fun subscribeActual(observer: Observer<in X>) {
            entrySubject.subscribe(ItemFilterOperator(observer,id))
        }
    }

    fun itemStream(id:String) : Observable<X> {
        return ItemObservable(id)
    }


    private inner class ListFilterOperator(private val observer:Observer<in List<X>>,private val predicate:(x:X)->Boolean) : Observer<Map<String,X?>> {
        lateinit var filteredMap : MutableMap<String, X>

        override fun onComplete() {
            observer.onComplete()
        }

        override fun onSubscribe(d: Disposable) {
            filteredMap = HashMap<String,X>(map.filter { (_, x) -> predicate(x)})
            observer.onSubscribe(d)
            observer.onNext(ArrayList(filteredMap.values))
        }

        override fun onNext(m: Map<String,X?>) {
            var hasChanged : Boolean = false
            for ((key, x) in m) {
                if (x!=null && predicate(x)) {
                    filteredMap.put(key, x)
                    hasChanged = true
                } else {
                    val old = filteredMap.remove(key)
                    hasChanged = hasChanged || old!=null
                }

            }

            if (hasChanged) {
                observer.onNext(ArrayList(filteredMap.values))
            }
        }

        override fun onError(e: Throwable) {
            observer.onError(e)
        }
    }



    private inner class FilteredListObservable (private val predicate:(x:X)->Boolean) : Observable<List<X>>() {
        override fun subscribeActual(observer: Observer<in List<X>>) {
            listSubject.subscribe(ListFilterOperator(observer,predicate))
        }
    }

    fun listStream(predicate:(x:X)->Boolean) : Observable<List<X>> {
        return FilteredListObservable(predicate)
    }
}

