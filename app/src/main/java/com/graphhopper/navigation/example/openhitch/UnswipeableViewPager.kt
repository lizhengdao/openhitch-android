package com.graphhopper.navigation.example.openhitch

import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent


class UnswipeableViewPager : androidx.viewpager.widget.ViewPager {
    constructor(context: Context, attrs: AttributeSet?) : super(context,attrs) {}
    constructor(context: Context) : super(context) {}
    override fun onTouchEvent(event: MotionEvent?): Boolean = false
    override fun onInterceptTouchEvent(event: MotionEvent?) = false
}