package com.graphhopper.navigation.example.openhitch

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView

import com.graphhopper.navigation.example.R

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.lang.RuntimeException
import java.util.*

class OfferActivity : HitchActivity() {
    lateinit var waypoints: ArrayList<OHPoint>
    var duration: Long = -1

    @BindView(R.id.all)
    lateinit var rootView: View

    @BindView(R.id.max_detour_spinner)
    lateinit var maxDetourSpinner: Spinner

    @BindView(R.id.free_seats_spinner)
    lateinit var freeSeatsSpinner: Spinner

    @BindView(R.id.start_point_view)
    lateinit var startPointTextView: TextView
    @BindView(R.id.destination_point_view)
    lateinit var destinationPointTextView: TextView

    @BindView(R.id.date_button)
    lateinit var dateButton: DateButton

    @BindView(R.id.start_time_button)
    lateinit var startTimeButton: TimeButton

    @BindView(R.id.submit_button)
    lateinit var submitButton: Button

    @BindView(R.id.collision_list_header)
    lateinit var collisionListHeader: TextView;

    lateinit var collisionListFragment: CollisionListFragment;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer)
        ButterKnife.bind(this)
        collisionListFragment = supportFragmentManager.findFragmentById(R.id.collision_list_fragment) as CollisionListFragment

        getParameters()
        initUi()
        updateCollisionList()
    }

    fun updateCollisionList() {
        val startTime = dateButton.time + startTimeButton.timeOfDay
        val endTime = startTime+duration
        hitchServiceConnection.whenReady { api->
            val observable = api.ways.collisionListStream(startTime, endTime).take(1)
            hitchServiceConnection.observe(observable) {
                collisionListFragment.updateItems(it)
                val n = it.size
                if(n==0) {
                    collisionListHeader.visibility = View.GONE
                } else {
                    collisionListHeader.text = "⚠️"+getResources().getQuantityString(R.plurals.collision_warning_search,n)+":"
                    collisionListHeader.visibility = View.VISIBLE
                }
            }
        }
    }


    private fun initUi() {
        initFreeSeatSpinner()
        initMaxDetourSpinner()
        initLocations()
        val now = Date()
        dateButton.init(now)
        startTimeButton.timeOfDay = Util.getTimeOfDay(now)
        startTimeButton.listener = {
            updateCollisionList()
        }
    }

    private fun initLocations() {
        startPointTextView.text = waypoints[0].description
        destinationPointTextView.text = Util.getLast(waypoints).description
    }

    private fun getParameters() {
        @Suppress("UNCHECKED_CAST")
        waypoints = intent.getSerializableExtra(PARAMETER_WAYPOINTS) as ArrayList<OHPoint>
        duration = intent.getLongExtra(PARAMETER_DURATION,-1L)
        if (duration==-1L) {
            throw RuntimeException("Route duration not given")
        }
    }

    private fun initFreeSeatSpinner() {
        val items = arrayOf(1, 2, 3, 4, 5, 6, 7, 8)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, items)
        freeSeatsSpinner.adapter = adapter
    }

    private fun initMaxDetourSpinner() {
        val items = arrayOf(1, 3, 5, 10, 15, 20, 30)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, items)
        maxDetourSpinner.adapter = adapter
        maxDetourSpinner.setSelection(2)
    }


    @OnClick(R.id.cancel_button)
    fun cancel() {
        finish()
        startActivity(Intent(this, MainActivity::class.java))
    }

    @OnClick(R.id.submit_button)
    fun submitButtonClick() {
        hitchServiceConnection.whenReady { api ->
            val user = api.authentication.user
            if (user == null) {
                val intent = Intent(this, AccountActivity::class.java)
                startActivityForResult(intent,AccountActivity.REQUEST_REGISTER)
            } else {
                submitButton.isEnabled = false
                val way = buildWay(user.id)
                api.createWay(way)
                val intent = Intent(this, WayDetailActivity::class.java)
                intent.putExtra(WayDetailActivity.PARAMETER_WAY_ID, way.id)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(AccountActivity.REQUEST_REGISTER, resultCode, data)
        if (resultCode== Activity.RESULT_OK) {
            Snackbar.make(rootView,R.string.registration_success, Snackbar.LENGTH_LONG).show()
        }
    }

    fun buildWay(user_id:String) : Way {
        val startTime = dateButton.time + startTimeButton.timeOfDay   // FIXME check: should not be in the past
        val maxDetourTime = ((maxDetourSpinner.selectedItem as Int)* MINUTE).toLong()

        return Way(
                id = Util.randomId(),
                status = Way.Status.PUBLISHED,
                user_id = user_id,
                role = Role.DRIVER,
                waypoints = waypoints,
                start_time = startTime,
                end_time = startTime + duration+maxDetourTime,
                seats = freeSeatsSpinner.selectedItem as Int,
                autocommit = false, // FIXME provide gui component
                max_detour_time = maxDetourTime
        )
    }

    companion object {
        val PARAMETER_DURATION = "duration"
        val PARAMETER_WAYPOINTS = "waypoints"

        private val FIRST_NAVIGATION_DIALOG_VERSION = 1
        val START_NOW = "startNow"
    }

}
