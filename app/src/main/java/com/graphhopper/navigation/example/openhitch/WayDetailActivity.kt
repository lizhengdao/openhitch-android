package com.graphhopper.navigation.example.openhitch

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.FragmentPagerAdapter
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.navi.NavigationPreparer
import com.graphhopper.navigation.example.openhitch.navi.WayMapFragment
import com.graphhopper.navigation.example.openhitch.service.api.Api
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.lang.RuntimeException


class WayDetailActivity : HitchActivity() {
    @BindView(R.id.header)
    lateinit var headerView: TextView

    @BindView(R.id.way_view)
    lateinit var wayView: WayView

    @BindView(R.id.cancel_button)
    lateinit var cancelButton: Button

    @BindView(R.id.close_button)
    lateinit var closeButton: Button

    @BindView(R.id.drive_button)
    lateinit var driveButton: Button

    @BindView(R.id.viewpager)
    lateinit var viewPager : androidx.viewpager.widget.ViewPager;

    @BindView(R.id.tabs)
    lateinit var tabLayout : TabLayout

    @BindView(R.id.collision_list_header)
    lateinit var collisionListHeader: TextView;

    lateinit var collisionListFragment: CollisionListFragment;


    private lateinit var wayId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.graphhopper.navigation.example.R.layout.activity_way_detail)
        ButterKnife.bind(this)
        collisionListFragment = supportFragmentManager.findFragmentById(R.id.collision_list_fragment) as CollisionListFragment

        val extras = intent.extras
        wayId = extras!!.getString(PARAMETER_WAY_ID)!!

        viewPager.adapter = PageAdapter(supportFragmentManager)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setupWithViewPager(viewPager)

    }

    override fun onStart() {
        super.onStart()
        viewPager.setCurrentItem(2, false)
    }

    internal val liftListFragment : LiftListFragment by lazy {
        var fragment = LiftListFragment()
        fragment.arguments = intent.extras
        fragment
    }

    internal val mapFragment : WayMapFragment by lazy {
        val fragment = WayMapFragment()
        fragment.arguments = intent.extras
        fragment
    }

    internal val waySettingsFragment : WaySettingsFragment by lazy {
        var fragment = WaySettingsFragment()
        fragment.arguments = intent.extras
        fragment
    }

    override fun onHitchServiceAttach(api: Api) {
        val way = api.ways.getWay(wayId)
        val idHeaderText = when(way.role) {
            Role.PASSENGER -> R.string.search_ride  // FIXME depend on state
            Role.DRIVER -> R.string.ride_offer
        }
        headerView.text = getText(idHeaderText)
        wayView.setWay(way)
        wayView.setBackgroundColor(ContextCompat.getColor(this, R.color.workingArea))
        val idTabLiftsText = when(way.role) {
            Role.PASSENGER -> R.string.lifts
            Role.DRIVER -> R.string.passengers
        }
        tabLayout.getTabAt(2)?.setText(idTabLiftsText)
        driveButton.visibility = if (way.role==Role.DRIVER) VISIBLE else GONE
        hitchServiceConnection.observe(api.ways.liftInfoCountMapStream(wayId)) {
            when (way.role) {
                Role.DRIVER -> this::setPartnerInfosDriver
                Role.PASSENGER -> this::setPartnerInfosPassenger
            }
        }
        val collisionListStream = api.ways.collisionListStream(wayId)
        hitchServiceConnection.observe(collisionListStream.map {it.size}) {
            if(it==0) {
                collisionListHeader.visibility = GONE
            } else {
                collisionListHeader.text = "⚠️"+getResources().getQuantityString(R.plurals.collision_warning_way,it)+":"
                collisionListHeader.visibility = VISIBLE
            }
        }
        hitchServiceConnection.observe(collisionListStream){
            collisionListFragment.updateItems(it)
        }
    }

    private fun setPartnerInfosDriver(m: Map<Lift.Status, Int?>) {
        // FIXME use getQuantityString for pluralisation
        val passengers = (m.get(Lift.Status.ACCEPTED)?:0) + (m.get(Lift.Status.STARTED)?:0)
        val requests = m.get(Lift.Status.REQUESTED)?:0
        val s = String.format(getString(R.string.passengers_requests_format),passengers,requests)
        wayView.partnerInfo.text = s
    }

    private fun setPartnerInfosPassenger(m: Map<Lift.Status, Int?>) {
        // FIXME use getQuantityString for pluralisation
        val accepted = m.get(Lift.Status.ACCEPTED)?:0 + (m.get(Lift.Status.STARTED)?:0)
        val requested = m.get(Lift.Status.REQUESTED)?:0
        val offered = m.get(Lift.Status.SUGGESTED)?:0
        val s:String = if (accepted>0) {
            getString(R.string.lift_agreed) + (if(offered>0) { ", "+String.format(getString(R.string.more_offers_available),offered) } else "");
        } else if (requested>0) {
            getString(R.string.lift_requested) + (if(offered>0) { ", "+String.format(getString(R.string.more_offers_available),offered) } else "");
        } else {
            String.format(getString(R.string.offers_available),offered)
        }
        wayView.partnerInfo.text = s
    }


    @OnClick(R.id.cancel_button)
    fun onCancelButtonClick() {
        hitchServiceConnection.whenReady { api ->
            api.deleteWay(wayId)
            finish()
        }
    }

    @OnClick(R.id.close_button)
    fun onCloseButtonClick() {
        finish()
    }


    @OnClick(com.graphhopper.navigation.example.R.id.drive_button)
    fun onDriveButtonClick() {
        // FIXME Warn driver if start is too early and there are passengers or if drive is scheduled for a later day
        launchNavigation()
    }

    private fun launchNavigation() {
        var navigationPreparer = NavigationPreparer(this, true)
        hitchServiceConnection.whenReady { api ->
            api.ways.waypointsStream(wayId).take(1).subscribe() {
                navigationPreparer.launchNavigation(it,true)
            }
        }
    }

    companion object {
        val PARAMETER_WAY_ID = "way_id"
    }


    inner class PageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private val numOfTabs: Int = 3

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> mapFragment
                1 -> waySettingsFragment
                2 -> liftListFragment
                else -> throw RuntimeException("Unexpected tab position $position")
            }

        }

        override fun getCount(): Int {
            return numOfTabs
        }

        override fun getPageTitle(position: Int): CharSequence? {
            // Generate title based on item position
            when (position) {
                0 -> return getString(R.string.map)
                1 -> return getString(R.string.data)
                2 -> return getString(R.string.passengers)
                else -> return null
            }
        }
    }

}