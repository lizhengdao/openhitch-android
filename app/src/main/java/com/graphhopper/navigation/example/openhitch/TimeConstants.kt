package com.graphhopper.navigation.example.openhitch

val SECOND = 1000
val MINUTE = 60*SECOND
val HOUR = 60*MINUTE
val DAY = 24*HOUR