package com.graphhopper.navigation.example.openhitch

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView

import com.graphhopper.navigation.example.R
import de.wifaz.oh.protocol.Lift

import java.util.Date

import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way

class LiftInfoView @JvmOverloads constructor( aContext: Context, attrs: AttributeSet? = null) : LinearLayout(aContext, attrs) {
    private val aView: View

    init {
        var layoutId = R.layout.view_liftinfo

        // cf. https://stacktips.com/tutorials/android/creating-custom-views-in-android-tutorial#5-apply-custom-attributes
        if (attrs != null) {
            val a = context.theme.obtainStyledAttributes(attrs,
                    R.styleable.LiftInfoView, 0, 0)

            layoutId = a.getResourceId(R.styleable.LiftInfoView_layout, R.layout.view_liftinfo)
        }

        // FIXME is this needed? How does the containing View notice?
        aView = View.inflate(aContext, layoutId, this)
    }

    fun setLiftInfo(liftInfo: LiftInfo, way: Way) {
        setText(R.id.header,liftTitle(context,liftInfo.lift.status,liftInfo.role))
        val nickname = liftInfo.partner.nick_name ?: liftInfo.partner.first_name + " " + liftInfo.partner.last_name
        setText(R.id.name_view, nickname)
        setText(R.id.destination_point_view, liftInfo.lift.drop_off_point.toString())
        setWarningPartial(liftInfo.lift,way)

        val detour_distance = String.format("%.1f km", liftInfo.lift.detour_distance / 1000)  // fixme localize
        val detour_time = String.format("%d minutes", liftInfo.lift.detour_time/ MINUTE)
        val pickup_time = Util.formatDateTime(context, Date(liftInfo.lift.pickup_time))
        val shared_distance = String.format("%.1f km", liftInfo.lift.shared_distance / 1000)  // fixme localize
        val price = Util.formatMoney(liftInfo.lift.price, liftInfo.lift.currency)
        val miscInfo = pickup_time+", "+shared_distance+", "+price
        setText(R.id.misc_info,miscInfo)
        setStatus(liftInfo)
    }

    private fun setWarningPartial(lift: Lift, way: Way) {
        val showWarning = way.role==Role.PASSENGER && lift.sectional
        val textView = aView.findViewById<TextView>(R.id.partial_lift_warning)
        if (showWarning) {
            textView.visibility = VISIBLE
            textView.text = "⚠️"+context.getString(R.string.warning_partial)
        } else {
            textView.visibility = GONE
        }
    }

    private fun setStatus(liftInfo: LiftInfo) {
        val rid : Int = when(liftInfo.lift.status) {
            Lift.Status.SUGGESTED -> R.string.lift_status_suggested
            Lift.Status.REQUESTED -> R.string.lift_status_requested
            Lift.Status.ACCEPTED -> R.string.lift_status_accepted
            Lift.Status.REJECTED -> R.string.lift_status_rejected
            Lift.Status.STARTED -> R.string.lift_status_started
            Lift.Status.FINISHED -> R.string.lift_status_finished
            Lift.Status.DRIVER_CANCELED -> R.string.lift_status_driver_canceled
            Lift.Status.PASSENGER_CANCELED -> R.string.lift_status_passenger_canceled
        }
        setText(R.id.status_view,context.getString(rid))
    }

    fun setText(textViewId: Int, text: String) {
        val textView = aView.findViewById<TextView>(textViewId)
        if (textView != null) {
            textView.text = text
        }
    }

}

