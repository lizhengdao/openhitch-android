package com.graphhopper.navigation.example.openhitch

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.service.api.Api
import de.wifaz.oh.protocol.Way
import java.util.*

class CollisionListFragment : ListFragment<Way>() {

    override fun onItemClick(item: Way) {
        var intent = Intent(activity,WayDetailActivity::class.java)
        intent.putExtra(WayDetailActivity.PARAMETER_WAY_ID,item.id)
        startActivity(intent)
        activity!!.finish()
        Intent()
    }

    fun updateItems(ways: List<Way>) {
        replaceItems(ways)
    }

    override fun createView(container: ViewGroup): View {
        return activity!!.layoutInflater.inflate(R.layout.item_collision, container, false)
    }

    override fun initItemView(view: View, item: Way) {
        val wayDescription = wayDescription(item)
        (view.findViewById<View>(R.id.waytitle) as TextView).text = wayDescription
    }

    private fun wayDescription(item: Way): String {
        val destination = item.waypoints.last().description
        val timeString = Util.formatDateTime(context!!, Date(item.start_time))
        val wayDescription = getString(R.string.collision_way_format).format(timeString, destination)
        return wayDescription
    }
}
