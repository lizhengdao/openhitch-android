package com.graphhopper.navigation.example.openhitch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.graphhopper.navigation.example.R
import timber.log.Timber


abstract class ListFragment<T> : HitchFragment(), OnItemClickListener {
    @BindView(R.id.list_view)
    lateinit var listView: ListView
    @BindView(R.id.loading)
    lateinit var loading: ProgressBar
    @BindView(R.id.empty_list_text_view)
    lateinit var emptyListTextView: TextView

    var items: List<T>? = null
        private set

    protected lateinit var adapter: BaseAdapter

    protected open val emptyText: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        ButterKnife.bind(this, view)
        return view
    }

    override fun onStart() {
        super.onStart()
        adapter = LocalListAdapter()
        listView.adapter = adapter
        listView.onItemClickListener = this
    }

    fun hideLoading() {
        if (loading.visibility == View.VISIBLE) {
            loading.visibility = View.GONE
        }
    }

    fun showLoading() {
        if (loading.visibility == View.INVISIBLE) {
            loading.visibility = View.VISIBLE
        }
    }

    /**
     *
     * @param items New item list. If items==null this call is ignored
     */
    protected fun replaceItems(items: List<T>?) {
        if (items == null) {
            Timber.d("items==null")
            return
        }
        this.items = items

        Timber.d("items.size()=%s", items.size)
        if (items.size == 0) {
            val emptyText = emptyText
            if (emptyText != null) {
                emptyListTextView.text = emptyText
                emptyListTextView.visibility = View.VISIBLE
                listView.visibility = View.GONE
            }
        } else {
            emptyListTextView.visibility = View.GONE
            listView.visibility = View.VISIBLE
        }
        adapter.notifyDataSetChanged()
        setListViewHeightBasedOnChildren()
        hideLoading()
    }

    override fun onItemClick(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
        @Suppress("UNCHECKED_CAST")
        val item = adapter.getItem(i) as T
        onItemClick(item)
    }

    protected abstract fun onItemClick(item: T)

    protected abstract fun createView(container: ViewGroup): View

    protected abstract fun initItemView(view: View, item: T)

    private fun setListViewHeightBasedOnChildren() {
        // see https://stackoverflow.com/questions/6071131/how-to-change-listview-height-dynamically-in-android
        var totalHeight = 0
        val desiredWidth = MeasureSpec.makeMeasureSpec(listView.width, MeasureSpec.AT_MOST)
        for (i in 0 until adapter.count) {
            val listItem = adapter.getView(i, null, listView)
            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED)
            totalHeight += listItem.measuredHeight
        }
        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (adapter.count - 1)
        listView.layoutParams = params
        listView.requestLayout()
    }

    protected inner class LocalListAdapter : BaseAdapter() {
        override fun getCount(): Int {
            return items?.size ?: 0
        }

        override fun getItem(i: Int): T? {
            return items!![i]
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun getView(position: Int, convertView: View?, container: ViewGroup): View? {
            var view = convertView ?: createView(container)

            val item = items!![position]

            initItemView(view, item)

            return view
        }
    }
}
