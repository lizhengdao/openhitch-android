package com.graphhopper.navigation.example.openhitch.navi

import android.content.Context
import android.location.Location
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.SimplifiedCallback
import com.graphhopper.navigation.example.openhitch.Preferences
import com.graphhopper.navigation.example.openhitch.Util
import com.graphhopper.navigation.example.openhitch.service.HitchServiceConnection
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.mapbox.turf.TurfMeasurement
import retrofit2.Call
import retrofit2.Response
import java.lang.Exception

fun fetchRoute(context : Context, waypoints: List<Point>?, lastKnownLocation: Location?, callback : HitchServiceConnection.Callback<DirectionsRoute> ) {
    var navigateApiUrl = context.getString(R.string.base_url) + "navigate/"  // FIXME

    // FIXME cache known Routes
    if (waypoints == null) {
        return
    }

    val builder = NavigationRoute.builder(context)
            .accessToken("pk." + context.getString(R.string.gh_key))
            .baseUrl(navigateApiUrl)
            .user("gh")
            .alternatives(true)

    val startPoint = waypoints[0]

    var startPointDone = false;
    if (lastKnownLocation!=null) {
        val location = Point.fromLngLat(lastKnownLocation.longitude, lastKnownLocation.latitude)
        if (lastKnownLocation.hasBearing() && TurfMeasurement.distance(startPoint, location) < 0.01) {
            // 90 seems to be the default tolerance of the SDK
            builder.origin(location, lastKnownLocation.bearing.toDouble(), 90.0)
            startPointDone = true
        }
    }
    if(!startPointDone){
        builder.origin(startPoint)
    }

    for (i in 1 until waypoints.size - 1) {
        builder.addWaypoint(waypoints[i])
    }
    builder.destination(Util.getLast(waypoints))


    setFieldsFromSharedPreferences(context, builder)
    builder.build().getRoute(object : SimplifiedCallback() {
        override fun onResponse(call: Call<DirectionsResponse>, response: Response<DirectionsResponse>) {
            response.body()?.routes()?.forEach {
                // We only want the first route (if any)
                callback.onResponse(it)
                return
            }

            // The iteration above was not entered, so there is no valid route
            callback.onFailure(Exception("No valid route returned by server"))  // FIXME need more sophisticated Exception hierarchy and localized error messages
        }

        override fun onFailure(call: Call<DirectionsResponse>, throwable: Throwable) {
            super.onFailure(call, throwable)
            callback.onFailure(throwable)
        }
    })
}

private fun setFieldsFromSharedPreferences(context : Context, builder: NavigationRoute.Builder) {
    builder
            .language(Preferences.getLocale(context))
            .voiceUnits(Preferences.getUnitType(context))
            .profile("driving")
}

