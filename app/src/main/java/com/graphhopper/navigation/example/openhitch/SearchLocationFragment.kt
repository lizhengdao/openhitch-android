package com.graphhopper.navigation.example.openhitch

import android.content.Context
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

import com.graphhopper.directions.api.client.model.GeocodingLocation
import com.graphhopper.navigation.example.FetchGeocodingConfig
import com.graphhopper.navigation.example.FetchGeocodingTask
import com.graphhopper.navigation.example.FetchGeocodingTaskCallbackInterface
import com.graphhopper.navigation.example.R
import com.mapbox.geojson.Point

import java.util.ArrayList

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnFocusChange
import com.graphhopper.navigation.example.openhitch.navi.ChooseWaypointsActivity
import com.graphhopper.navigation.example.openhitch.navi.SearchBox
import com.graphhopper.navigation.example.openhitch.navi.toOHPoint
import de.wifaz.oh.protocol.OHPoint
import timber.log.Timber

class SearchLocationFragment : Fragment(), AdapterView.OnItemClickListener, TextView.OnEditorActionListener, View.OnFocusChangeListener {
    @BindView(R.id.icon)
    lateinit var icon: ImageView
    @BindView(R.id.label)
    lateinit var label: TextView
    @BindView(R.id.edit_text)
    lateinit var editText: AutoCompleteTextView
    @BindView(R.id.search_button)
    lateinit var searchButton: ImageButton
    @BindView(R.id.reset_button)
    lateinit var resetButton: ImageButton

    private lateinit var rootView: View
    private var options: MutableList<OHPoint> = ArrayList()
    private var filteredOptions: List<OHPoint> = ArrayList()
    private var adapter: SearchAdapter
    private lateinit var searchBox: SearchBox
    private var index: Int = 0
    internal var waypoint: OHPoint? = null
    private var isInitialized: Boolean = false

    init {
        adapter = SearchAdapter()
    }

    val waypointDescription: String
        get() = editText.text.toString()

    private val currentLocation: OHPoint?
        get() {
            val lastKnownLocation = (activity as ChooseWaypointsActivity).mapFragment.lastKnownLocation ?: return null
            val result = lastKnownLocation.toOHPoint()
            result?.description = CURRENT_POSITION_MARKER + activity!!.getString(R.string.current_location)
            return result
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_search, container, false)
        ButterKnife.bind(this, rootView)
        return rootView
    }

    fun onMapReady(searchBox: SearchBox, index: Int) {
        this.searchBox = searchBox
        this.index = index

        // String[] places = {"\u2295 Standort", "Stuttgart", "Moritstraße", "Hardenbergplatz", "Fichtenweg"};
        editText.threshold = 0
        editText.setAdapter<SearchAdapter>(adapter)
        editText.onItemClickListener = this
        editText.setOnEditorActionListener(this)

        rootView.visibility = View.VISIBLE
        isInitialized = true

        // editText.setOnFocusChangeListener(this)
    }

    fun focus() {
        editText.requestFocus()
    }

    override public fun onFocusChange( view:View, focused: Boolean) {
        if (focused) {
            searchBox.activity.isKeyboardShowing = true;
        }
    }

    @OnClick(R.id.edit_text)
    fun onEditTextClick() {
        searchBox.activity.isKeyboardShowing = true;
    }

    @OnClick(R.id.search_button)
    fun onSearchButtonClick() {
        startSearch()

        searchBox.activity.isKeyboardShowing = false;
    }

    @OnClick(R.id.reset_button)
    fun onResetButtonClick() {
        editText.setText("")
        resetOptions()
        focus()
    }

    @OnClick(R.id.icon)
    fun onIconClick() {
        focus()
    }

    private fun startSearch() {
        val currentInput = editText.text.toString()
        (activity as ChooseWaypointsActivity).mapFragment.showLoading()

        val pointLatLng = searchBox.position
        val point = pointLatLng.toString()
        val language = Preferences.getLocale(context!!).language
        val geocodingConfig = FetchGeocodingConfig(currentInput, language, 5, false, point, "default")
        val callback = object : FetchGeocodingTaskCallbackInterface {
            override fun onError(messageResource: Int) {
                Snackbar.make(view!!, messageResource, Snackbar.LENGTH_LONG).show()
            }

            override fun onPostExecuteGeocodingSearch(locations: List<GeocodingLocation>) {
                if (locations.isEmpty()) {
                    onError(R.string.error_geocoding_no_location)
                    (activity as ChooseWaypointsActivity).mapFragment.hideLoading()
                    return
                }

                options.clear()
                for (location in locations) {
                    val snippet = formatLocation(location)
                    val gcpoint = location.point
                    val option = OHPoint.fromLngLat(gcpoint.lng!!, gcpoint.lat!!)
                    option.description = snippet
                    options.add(option)
                    addCurrentLocationOption(options)
                }
                (activity as ChooseWaypointsActivity).mapFragment.hideLoading()
                // adapter.notifyDataSetChanged();
                adapter.filter.filter(null)
                editText.showDropDown()
            }
        }
        FetchGeocodingTask(callback, getString(R.string.gh_key)).execute(geocodingConfig)
    }

    private fun addCurrentLocationOption(options: MutableList<OHPoint>) {
        val currentLocation = currentLocation
        if (currentLocation != null) {
            options.add(currentLocation)
        }

    }

    fun startReverseSearch(point: Point) {
        val pointAsString = String.format("%.5f,%.5f", point.latitude(), point.longitude())
        val language = Preferences.getLocale(context!!).language
        val geocodingConfig = FetchGeocodingConfig(null, language, 5, true, pointAsString, "default")

        val callback = object : FetchGeocodingTaskCallbackInterface {
            override fun onError(message: Int) {
                Timber.e( "Error in reverse geocoding")
            }

            override fun onPostExecuteGeocodingSearch(points: List<GeocodingLocation>) {
                if (points.size == 0) {
                    return
                }
                val snippet = formatLocation(points[0])
                val ohPoint = waypoint
                if (ohPoint == null) {
                    Timber.e( "Waypoint shoud be set")
                } else {
                    ohPoint.description = snippet
                }
                editText.setText(snippet)
                editText.dismissDropDown()
            }
        }

        FetchGeocodingTask(callback, getString(R.string.gh_key)).execute(geocodingConfig)
    }

    @OnFocusChange(R.id.edit_text)
    fun onFocusChanged(focused: Boolean) {
        if (!isInitialized) {
            return
        }
        if (focused) {
            val text = editText.text.toString()
            if (Util.isEmpty(text) || text.startsWith(CURRENT_POSITION_MARKER)) {
                resetOptions()
            } else {
                adapter.filter.filter(null)
            }
            searchBox.setFocusIndex(index)
            editText.showDropDown()
            val color = ContextCompat.getColor(context!!,R.color.workingAreaHighlight)
            rootView.setBackgroundColor(color)
        } else {
            val color = ContextCompat.getColor(context!!,R.color.workingArea)
            rootView.setBackgroundColor(color)
        }
    }

    private fun resetOptions() {
        options.clear()
        options.addAll(searchBox.favorites.toList())
        addCurrentLocationOption(options)
        adapter.filter.filter(null)
        adapter.notifyDataSetChanged()
    }

    private fun formatLocation(location: GeocodingLocation): String {
        var snippet = ""
        if (location.street != null) {
            snippet += location.street
            if (location.housenumber != null) {
                snippet += " " + location.housenumber
            }
        }
        var city: String? = location.city
        if (city == null) {
            city = location.state
        }
        if (city != null) {
            if ("" != snippet) {
                snippet += ", "
            }
            if (location.postcode != null) {
                snippet += location.postcode + " "
            }
            snippet += city
        }
        if (snippet.isEmpty()) {
            snippet += location.name
        }
        return snippet
    }

    override fun onEditorAction(textView: TextView, i: Int, keyEvent: KeyEvent): Boolean {
        // FIXME startSearch()
        return true
    }

    override fun onItemClick(adapterView: AdapterView<*>, view: View?, i: Int, l: Long) {
        val ohPoint = adapterView.adapter.getItem(i) as OHPoint
        waypoint = ohPoint
        searchBox.updateRoute()
        if (!(ohPoint.description?:"").startsWith(CURRENT_POSITION_MARKER)) {
            searchBox.addFavorite(ohPoint)
        }
    }

    fun getWaypoint(): OHPoint? {
        return waypoint
    }

    fun setWaypoint(point: OHPoint) {
        this.waypoint = point
        searchBox.updateRoute()
    }

    fun setIcon(iconResourceId: Int) {
        icon.setImageDrawable(ContextCompat.getDrawable(context!!,iconResourceId))
    }

    fun setLabel(stringResourceId: Int, colorResourceId: Int) {
        label.setText(stringResourceId)
        label.setTextColor(ContextCompat.getColor(context!!,colorResourceId))
    }


    private inner class SearchFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()

            val values = ArrayList<OHPoint>()
            if (constraint == null || constraint.length == 0) {
                values.addAll(options)
            } else {
                val words = constraint.toString().trim { it <= ' ' }.toLowerCase().split("\\W".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

                outerloop@ for (option in options) {
                    val s = (option.description?:"").toLowerCase()
                    for (word in words) {
                        if (s.indexOf(word) == -1) {
                            continue@outerloop
                        }
                    }
                    values.add(option)
                    if (values.size > 10) {
                        break
                    }
                }

            }
            results.values = values
            results.count = values.size
            return results
        }

        override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
            @Suppress("UNCHECKED_CAST")
            filteredOptions = filterResults.values as List<OHPoint>
            adapter.notifyDataSetChanged()
        }
    }

    fun setToCurrentLocation() {
        val option = currentLocation
        if (option != null) {
            editText.setText(option.description)
            setWaypoint(option)
        }
    }

    private inner class SearchAdapter : BaseAdapter(), Filterable {
        override fun getCount(): Int {
            return filteredOptions.size
        }

        override fun getItem(i: Int): Any {
            return filteredOptions[i]
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
            var inflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val textView = inflater.inflate(R.layout.autocomplete_item, null) as TextView
            textView.text = filteredOptions[i].description
            return textView
        }

        override fun getFilter(): Filter {
            return SearchFilter()
        }
    }

    companion object {
        private val CURRENT_POSITION_MARKER = "\u2295 "
    }
}