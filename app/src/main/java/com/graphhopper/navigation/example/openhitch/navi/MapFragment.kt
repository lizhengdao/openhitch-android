package com.graphhopper.navigation.example.openhitch.navi

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.CallSuper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat

import com.graphhopper.navigation.example.GHAttributionDialogManager
import com.graphhopper.navigation.example.R
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.core.constants.Constants
import com.mapbox.geojson.LineString
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.exceptions.InvalidLatLngBoundsException
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode

import java.util.ArrayList

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnItemClick
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.graphhopper.navigation.example.openhitch.navi.ChooseWaypointsActivity.Companion.CAMERA_ANIMATION_DURATION
import com.graphhopper.navigation.example.openhitch.HitchFragment
import com.graphhopper.navigation.example.openhitch.service.api.Api
import com.mapbox.android.core.location.LocationEngineListener
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.mapboxsdk.style.expressions.Expression
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource

open class MapFragment : HitchFragment(), OnMapReadyCallback, PermissionsListener {
    private val padding = intArrayOf(50, 50, 50, 50)

    @BindView(R.id.mapView)
    lateinit var mapView: MapView
    @BindView(R.id.loading)
    lateinit var loading: ProgressBar
    @BindView(R.id.current_location_fab)
    lateinit var currentLocationFab: FloatingActionButton

    private var locationLayer: LocationLayerPlugin? = null
    lateinit var mapboxMap: MapboxMap
    private lateinit var permissionsManager: PermissionsManager

    private var externalOnMapReadyCallback: OnMapReadyCallback? = null

    val lastKnownLocation: Location?
        @SuppressLint("MissingPermission")
        get() = locationLayer?.lastKnownLocation

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        ButterKnife.bind(this, view)

        return view
    }

    @CallSuper
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initMap(savedInstanceState)
    }

    private fun initMap(savedInstanceState: Bundle?) {
        mapView.setStyleUrl(getString(R.string.map_view_styleUrl)) // FIXME
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }

    private fun initDottedLineSourceAndLayer() {
        val dashedLineDirectionsFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        mapboxMap.addSource(GeoJsonSource("SOURCE_ID", dashedLineDirectionsFeatureCollection))
        mapboxMap.addLayerBelow(
                LineLayer(
                        "DIRECTIONS_LAYER_ID", "SOURCE_ID").withProperties(
                        PropertyFactory.lineWidth(4.5f),
                        PropertyFactory.lineColor(Color.BLACK),
                        PropertyFactory.lineTranslate(arrayOf(0f, 4f)),
                        PropertyFactory.lineDasharray(arrayOf(1.2f, 1.2f))
                ), "road-label-small")
    }

    /**
     * Add the route layer to the map either using the custom style values or the default.
     */
    private fun addRouteLayer(layerId: String, sourceId: String, color: Int) {
        val scale = 1f
        val featureCollection = FeatureCollection.fromFeatures(arrayOf())
        mapboxMap.addSource(GeoJsonSource(sourceId, featureCollection))
        val routeLayer = LineLayer(layerId, sourceId).withProperties(
                PropertyFactory.lineCap(Property.LINE_CAP_ROUND),
                PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND),
                PropertyFactory.lineWidth(Expression.interpolate(
                        Expression.exponential(1.5f), Expression.zoom(),
                        Expression.stop(4f, 3f * scale),
                        Expression.stop(10f, 4f * scale),
                        Expression.stop(13f, 6f * scale),
                        Expression.stop(16f, 10f * scale),
                        Expression.stop(19f, 14f * scale),
                        Expression.stop(22f, 18f * scale)
                )
                ),
                PropertyFactory.lineColor(color))
        mapboxMap.addLayerBelow(routeLayer, "road-label-small")
    }

    fun drawRoute(route: DirectionsRoute, sourceId: String) {
        val featureList = ArrayList<Feature>()
        val lineString = LineString.fromPolyline(route.geometry()!!, Constants.PRECISION_6)
        val coordinates = lineString.coordinates()
        for (i in coordinates.indices) {
            featureList.add(Feature.fromGeometry(LineString.fromLngLats(coordinates)))
        }
        val featureCollection = FeatureCollection.fromFeatures(featureList)
        val source = mapboxMap.getSourceAs<GeoJsonSource>(sourceId)
        source?.setGeoJson(featureCollection)
        boundCameraToRoute(route)
    }

    private fun initLocationLayer() {
        locationLayer = LocationLayerPlugin(mapView, mapboxMap)
        locationLayer!!.renderMode = RenderMode.COMPASS
        val lastKnownLocation = lastKnownLocation
        if (lastKnownLocation == null) {
            // show all of Germany
            animateCameraBbox(LatLngBounds.from( 55.2,5.8,47.3,15.0),CAMERA_ANIMATION_DURATION)
        } else {
            animateCamera(LatLng(lastKnownLocation.latitude, lastKnownLocation.longitude))
        }
        locationLayer!!.locationEngine!!.addLocationEngineListener(myLocationEngineListener())
    }

    fun hideLoading() {
        if (loading.visibility == View.VISIBLE) {
            loading.visibility = View.INVISIBLE
        }
    }

    fun showLoading() {
        if (loading.visibility == View.INVISIBLE) {
            loading.visibility = View.VISIBLE
        }
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            initLocationLayer()
        } else {
            Toast.makeText(context, getString(R.string.no_location_permission),
                    Toast.LENGTH_LONG).show()
        }
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        Toast.makeText(context, "This app needs location permissions to work properly.",
                Toast.LENGTH_LONG).show()
    }

    fun boundCameraToRoute(route: DirectionsRoute) {
        val routeCoords = LineString.fromPolyline(route.geometry()!!,
                Constants.PRECISION_6).coordinates()
        val bboxPoints = ArrayList<LatLng>()
        for (point in routeCoords) {
            bboxPoints.add(LatLng(point.latitude(), point.longitude()))
        }
        if (bboxPoints.size > 1) {
            try {
                val bounds = LatLngBounds.Builder().includes(bboxPoints).build()
                // left, top, right, bottom
                animateCameraBbox(bounds, CAMERA_ANIMATION_DURATION)
            } catch (exception: InvalidLatLngBoundsException) {
                Toast.makeText(context, R.string.error_valid_route_not_found, Toast.LENGTH_SHORT).show()
            }

        }
    }

    fun animateCameraBbox(bounds: LatLngBounds, animationTime: Int) {
        val position = mapboxMap.getCameraForLatLngBounds(bounds, padding)
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), animationTime)
    }

    fun animateCamera(point: LatLng) {
        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, DEFAULT_CAMERA_ZOOM.toDouble()), CAMERA_ANIMATION_DURATION)
    }

    @OnClick(R.id.current_location_fab)
    fun onCurrentLocationButtonClick() {
        lastKnownLocation?.let {
            animateCamera(LatLng(it.latitude, it.longitude))
        } ?: run {
            var stringId = if (!PermissionsManager.areLocationPermissionsGranted(context)) {
                R.string.no_location_permission
            } else {
                R.string.current_location_unknown
            }
            Snackbar.make(mapView,stringId,Snackbar.LENGTH_LONG).show()
        }
    }

    inner class myLocationEngineListener : LocationEngineListener {
        @RequiresApi(Build.VERSION_CODES.M)
        override fun onLocationChanged(location: Location?) {
            if (location==null) {
                currentLocationFab.setImageResource(R.drawable.ic_current_location_inactive_24)
            } else {
                currentLocationFab.setImageResource(R.drawable.ic_current_location_24)
            }
        }

        override fun onConnected() {
            //
        }

    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
        locationLayer?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
        locationLayer?.onStop()
    }

    override fun onDestroyView() {
        mapView.onDestroy()
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    @CallSuper
    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        this.mapboxMap.uiSettings.attributionDialogManager = GHAttributionDialogManager(this.mapView.context, this.mapboxMap)
        addRouteLayer(PASSENGER_ROUTE_LAYER_ID, PASSENGER_ROUTE_SOURCE_ID, ContextCompat.getColor(context!!,R.color.passengerRouteColor))
        addRouteLayer(DRIVER_ROUTE_LAYER_ID, DRIVER_ROUTE_SOURCE_ID, ContextCompat.getColor(context!!,R.color.driverRouteColor))

        // Check for location permission
        permissionsManager = PermissionsManager(this)
        if (!PermissionsManager.areLocationPermissionsGranted(context)) {
            permissionsManager.requestLocationPermissions(activity)
        } else {
            initLocationLayer()
        }
        externalOnMapReadyCallback?.onMapReady(mapboxMap)
    }

    fun setOnMapReadyCallback(callback: OnMapReadyCallback) {
        externalOnMapReadyCallback = callback
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    companion object {
        val DRIVER_ROUTE_SOURCE_ID = "driver-route-source"
        val PASSENGER_ROUTE_SOURCE_ID = "passenger-route-source"

        private val DEFAULT_CAMERA_ZOOM = 16
        private val DRIVER_ROUTE_LAYER_ID = "driver-route-layer"
        private val PASSENGER_ROUTE_LAYER_ID = "passenger-route-layer"
    }
}
