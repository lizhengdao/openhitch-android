package com.graphhopper.navigation.example.openhitch.service.api

import com.graphhopper.navigation.example.openhitch.service.Alerts
import com.graphhopper.navigation.example.openhitch.service.Ways
import de.wifaz.oh.protocol.*
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.lang.RuntimeException

/**
 * Methods marked with this annotation are intended to be used only only from the Api-code itself and not
 * from GUI-code. They should be also declared internal and are waiting for a refactoring that puts the Api into
 * a separate module or project.
 */
annotation class ApiInternal


open class ApiException(message: String) : RuntimeException(message) {}

class Api(val authentication: Authentication, private val messageStream: Observable<HitchMessage>, val hitchInterface: HitchInterface) {
    init {
        messageStream.subscribe() {
            try {
                digest(it)
            } catch( exc:ApiException) {
                Timber.e(exc,"Could not handle message from server due to inconsistency")
            }
        }
    }

    private fun digest(message: HitchMessage?) {
        if (message is LiftInfoMessage) {
            ways.putLiftInfos(message.reference_way_id,message.lift_infos)
        } else if (message is LiftStatusMessage) {
            ways.changeLiftStatus(message.reference_way_id,message.lift_id,message.status,byServerMessage = true)
        } else if (message is WayStatusMessage) {
            ways.changeWayStatus(message.way_id,message.status)
        }
    }

    val alerts = Alerts()
    val ways = Ways(this)

    fun changeLiftStatus(way_id:String, lift_id:String, newStatus: Lift.Status) {
        try {
            ways.changeLiftStatus(way_id, lift_id, newStatus)
            val call = hitchInterface.updateLiftStatus(lift_id, newStatus)
            enqueueCall(call, String.format("Uploading new status %s for lift %s", newStatus.name, lift_id))
        } catch (e: ApiException) {
            Timber.e(e,"ModelException %s",e.message)
        }
    }

    fun changeWayStatus(way_id:String, newStatus: Way.Status) {
        try {
            ways.changeWayStatus(way_id,newStatus)
            val call = hitchInterface.updateWayStatus(way_id, newStatus)
            enqueueCall(call, String.format("Uploading new status %s for way %s", newStatus.name, way_id))
        } catch (e: ApiException) {
            Timber.e(e,"ModelException %s",e.message)
        }
    }

    fun deleteWay(wayId: String) {
        val call = hitchInterface.deleteWay(wayId)
        enqueueCall(call, String.format("Deleting way %s",wayId))

        ways.removeWay(wayId)
    }

    fun createWay(way: Way) {
        ways.putWay(way)
        val call = hitchInterface.createWay(way)
        enqueueCall(call, String.format("Uploading new way %s", way.toString()))
    }

    private fun <T>enqueueCall(call: Call<T>, description: String, callback:((T)->Unit)?=null) {
        // FIXME allow HitchhMessage directly in response
        Timber.d("$description:_$call")
        call.enqueue(object : Callback<T> {

            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    Timber.d("call successful (%s)", description)
                    if (callback!=null) {
                        val body = response.body()
                        if (body==null) {
                            Timber.e("call returned no body (%s), status: %d, message: %s", description, response.code(), response.message())
                            return
                        }
                        callback(body)
                    }
                } else {
                    Timber.e("call failed (%s), status: %d, message: %s", description, response.code(), response.message())
                    // error response, no access to resource?
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                // something went completely south (like no internet connection)
                Timber.e(t,"call failed (%s)", description)
            }
        })
    }

    fun loadSyncDataFromServer() {
        val userId = authentication.user?.id
        if (userId==null) {
            return
        }
        val call = hitchInterface.getSyncData(userId)
        enqueueCall(call,"Getting sync data from server",ways::receiveSyncData)
    }
}