package com.graphhopper.navigation.example.openhitch.navi

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.LiftDetailActivity
import com.graphhopper.navigation.example.openhitch.Util
import com.graphhopper.navigation.example.openhitch.service.api.Api
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.util.*

class LiftMapFragment : MapFragment() {
    private lateinit var wayId: String
    private lateinit var liftId: String

    protected lateinit var liftInfo: LiftInfo

    private var driverMapRoute: NavigationMapRoute? = null
    private var passengerMapRoute: NavigationMapRoute? = null



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        wayId = arguments!!.getString(LiftDetailActivity.PARAMETER_WAY_ID)!!
        liftId = arguments!!.getString(LiftDetailActivity.PARAMETER_LIFT_ID)!!
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        super.onMapReady(mapboxMap)
        driverMapRoute = NavigationMapRoute(mapView, mapboxMap)
        passengerMapRoute = NavigationMapRoute(mapView, mapboxMap)
        hitchServiceConnection.whenReady { api ->
            hitchServiceConnection.observe(api.ways.liftInfoStream(wayId,liftId)) {
                liftInfo:LiftInfo-> showRoutes(liftInfo,api)
            }
        }
    }

    private fun showRoutes(liftInfo: LiftInfo, api: Api) {
        var way = api.ways.getWay(wayId)
        var driverWay : Way
        var passengerWay : Way
        when(way.role) {
            Role.DRIVER-> {
                driverWay = way
                passengerWay = liftInfo.partner_way
            }
            Role.PASSENGER-> {
                driverWay = liftInfo.partner_way
                passengerWay = way
            }
        }

        fetchRoute(context!!, passengerWay.waypoints.map {it.toPoint()}, null, hitchServiceConnection.Callback<DirectionsRoute>({drawRoute(it, PASSENGER_ROUTE_SOURCE_ID)}, this::onRouteFailure))
        val originalDriverWaypoints = driverWay.waypoints
        val driverWaypoints = Arrays.asList(originalDriverWaypoints[0], passengerWay.waypoints[0], Util.getLast(originalDriverWaypoints))
        fetchRoute(context!!, driverWaypoints.map { it.toPoint() }, null, hitchServiceConnection.Callback<DirectionsRoute>({drawRoute(it, DRIVER_ROUTE_SOURCE_ID)}, this::onRouteFailure))
    }

    private fun onRouteFailure(throwable: Throwable) {
        Snackbar.make(mapView, getString(R.string.error_calculating_route)+": "+throwable.message, Snackbar.LENGTH_LONG).show()
        hideLoading()
    }

}