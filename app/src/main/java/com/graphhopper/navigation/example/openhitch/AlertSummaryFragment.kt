package com.graphhopper.navigation.example.openhitch

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat

import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.service.Alert
import com.graphhopper.navigation.example.openhitch.service.api.Api
import com.graphhopper.navigation.example.openhitch.service.api.AlertSummaryItem

import java.util.ArrayList

class AlertSummaryFragment : ListFragment<AlertSummaryItem>() {
    override fun onHitchServiceAttach(api: Api) {
        hitchServiceConnection.observe(api.alerts.alertSummaryStream()) {
            update(it)
        }

    }

    override fun onItemClick(item: AlertSummaryItem) {
        // FIXME show list,  if(item.count!=1)
        val alert = item.alert
        val intent : Intent? = when(alert) {
            is Alert.Request -> {
                var intent = Intent(activity, LiftDetailActivity::class.java)
                intent.putExtra(LiftDetailActivity.PARAMETER_WAY_ID, alert.way_id)
                intent.putExtra(LiftDetailActivity.PARAMETER_LIFT_ID, alert.lift_id)
                intent
            }
            is Alert.Offer -> {
                var intent = Intent(activity, LiftDetailActivity::class.java)
                intent.putExtra(LiftDetailActivity.PARAMETER_WAY_ID, alert.way_id)
                intent.putExtra(LiftDetailActivity.PARAMETER_LIFT_ID, alert.lift_id)
                intent
            }
        }

        startActivity(intent)
    }

    private fun update(alertSummaryItems: List<AlertSummaryItem>) {
        replaceItems(ArrayList<AlertSummaryItem>(alertSummaryItems))
    }

    override fun createView(container: ViewGroup): View {
        return activity!!.layoutInflater.inflate(R.layout.item_alert, listView, false)
    }

    override fun initItemView(view: View, item: AlertSummaryItem) {
        val textView = view as TextView
        setItemText(textView,item)
        setItemColor(textView, item)
    }

    private fun setItemColor(textView: TextView, item: AlertSummaryItem) {
        val alert = item.alert
        val colorResourceId: Int = when (alert) {
            is Alert.Request -> R.color.liftRequestAlert
            is Alert.Offer -> R.color.liftOfferAlert
        }
        textView.setBackgroundColor(ContextCompat.getColor(context!!,colorResourceId))
    }

    private fun setItemText(textView:TextView, item: AlertSummaryItem) {
        val alert = item.alert
        val descriptionResourceId: Int = when (alert) {
            is Alert.Request -> R.plurals.liftRequestAlertDescription
            is Alert.Offer -> R.plurals.liftOfferAlertDescription
        }
        val format = resources.getQuantityString(descriptionResourceId, item.count)
        textView.text = String.format(format, item.count)
    }
}
