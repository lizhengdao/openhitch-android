package com.graphhopper.navigation.example.openhitch.navi

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.graphhopper.navigation.example.Constants
import com.graphhopper.navigation.example.R
import com.graphhopper.navigation.example.openhitch.SearchLocationFragment
import com.graphhopper.navigation.example.openhitch.Util
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.turf.TurfMeasurement
import de.wifaz.oh.protocol.OHPoint
import timber.log.Timber
import java.io.IOException
import java.util.*


class SearchBox {

    private lateinit var searchLocationFragments: MutableList<SearchLocationFragment>
    internal var favorites: MutableList<OHPoint> = ArrayList()

    public lateinit var activity: ChooseWaypointsActivity
    private lateinit var mapView: MapView
    private lateinit var mapboxMap: MapboxMap
    private var focusIndex: Int = 0

    val waypoints: ArrayList<OHPoint>?
        get() {
            val startPoint = getWayPoint(0)
            val destinationPoint = destinationPoint
            return if (startPoint == null || destinationPoint == null) {
                null
            } else ArrayList(Arrays.asList(startPoint, destinationPoint))
        }

    private val destinationPoint: OHPoint?
        get() = Util.getLast(searchLocationFragments).waypoint

    val position: LatLng
        get() = mapboxMap.cameraPosition.target


    private fun loadFavorites() {
        val preferences = activity.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, MODE_PRIVATE)
        val json = preferences.getString("favorites", null)
        if (json == null) {
            favorites = ArrayList()
        } else {
            val mapper = ObjectMapper()
            try {
                favorites = ArrayList<OHPoint>(mapper.readValue(json, object : TypeReference<List<OHPoint>>() {}));
                // val collectionType = mapper.typeFactory.constructCollectionType(List<*>::class.java!!, OHPoint::class.java!!)
                // favorites = mapper.readValue<List<OHPoint>>(json, collectionType)
            } catch (e: IOException) {
                Timber.e(e,"Could not convert stored favorites to java object")
            }

        }
    }

    private fun saveFavorites() {
        val mapper = ObjectMapper()
        try {
            val json = mapper.writeValueAsString(favorites)
            val editor = activity.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, MODE_PRIVATE).edit()
            editor.putString("favorites", json)
            editor.commit()
        } catch (e: JsonProcessingException) {
            Timber.e(e, "Error on writing favorites to json String")
        }

    }

    fun addFavorite(option: OHPoint) {
        for (i in favorites.indices) {
            val favorite = favorites[i]
            val p1 = option.toPoint()
            val p2 = favorite.toPoint()
            if (option.description == favorite.description && TurfMeasurement.distance(p1, p2) < 0.01) {
                favorites.removeAt(i)
                break
            }
        }
        if (favorites.size >= MAX_FAVORITES) {
            favorites.removeAt(favorites.size - 1)
        }
        favorites.add(0, option)
    }

    fun init(activity: ChooseWaypointsActivity) {
        this.activity = activity
        mapboxMap = activity.mapboxMap
        mapView = activity.mapView

        loadFavorites()
        clear()
    }

    fun getFragment(resourceId: Int): SearchLocationFragment {
        val fm = activity.supportFragmentManager
        val searchLocationFragment = fm.findFragmentById(resourceId) as SearchLocationFragment
        searchLocationFragment.onMapReady(this, searchLocationFragments.size)
        return searchLocationFragment
    }

    fun exit() {
        saveFavorites()
    }

    fun clear() {
        searchLocationFragments = ArrayList()
        val start = getFragment(R.id.start_chooser)
        searchLocationFragments.add(start)
        val destination = getFragment(R.id.destination_chooser)
        searchLocationFragments.add(destination)
        start.setIcon(R.drawable.ic_flag_green_24dp)
        start.setLabel(R.string.label_start, R.color.startFlagColor)
        start.setToCurrentLocation()
        destination.setIcon(R.drawable.ic_flag_red_24dp)
        destination.setLabel(R.string.label_destination, R.color.destinationFlagColor)
        setFocus(searchLocationFragments.size - 1)
    }

    private fun setFocus(index: Int) {
        setFocusIndex(index)
        searchLocationFragments[index].focus()
    }

    fun setFocusIndex(index: Int) {
        focusIndex = index
    }

    private fun getWayPoint(index: Int): OHPoint? {
        return searchLocationFragments[index].waypoint
    }


    fun updateRoute() {
        activity.updateRoute(waypoints?.map {it.toPoint()})
    }

    fun getFavorites(): List<OHPoint>? {
        return favorites
    }

    fun setPoint(point: Point) {
        val searchLocationFragment = searchLocationFragments[focusIndex]
        searchLocationFragment.waypoint = point.toOHPoint()
        searchLocationFragment.startReverseSearch(point)
    }

    companion object {
        private val MAX_FAVORITES = 100
    }
}
