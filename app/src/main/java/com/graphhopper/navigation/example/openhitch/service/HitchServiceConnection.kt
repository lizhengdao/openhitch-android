package com.graphhopper.navigation.example.openhitch.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.graphhopper.navigation.example.openhitch.service.api.Api
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

class HitchServiceConnection internal constructor(private val client: Client) : ServiceConnection {
    lateinit var context: Context

    public interface Client {
        fun onHitchServiceAttach(api: Api);
    }

    private var actions: MutableList<(api: Api)->Unit> = ArrayList()
    private var disposables: CompositeDisposable = CompositeDisposable()
    private var api: Api? = null

    fun open(context: Context) {
        this.context = context
        val intent = Intent(context, HitchService::class.java)
        context.bindService(intent, this, Context.BIND_AUTO_CREATE)
        Timber.d( "hitchServiceConnection %s bound",System.identityHashCode(this))

    }

    fun close() {
        Timber.d( "Disposing disposables of hitchServiceConnection %s", System.identityHashCode(this))
        disposables.clear()
        api = null  // FIXME is this properly synchronized with onServiceConnected()
        Timber.d( "unbinding hitchServiceConnection %s", System.identityHashCode(this))
        context.unbindService(this)
    }

    override fun onServiceDisconnected(p0: ComponentName?) {

    }

    @Synchronized
    override fun onServiceConnected(className: ComponentName,
                                    service: IBinder) {
        // We've bound to LocalService, cast the IBinder and get LocalService instance
        val binder = service as HitchService.LocalBinder
        val hitchService = binder.service

        api = hitchService.api
        client.onHitchServiceAttach(hitchService.api)
        callDeferredActions(hitchService.api)
    }

    inner class AutoDisposingObserver<T>(private val action: (t:T)->Unit) : Observer<T> {
        override fun onSubscribe(d: Disposable) {
            disposables.add(d)
        }

        override fun onNext(t: T) {
            action(t)
        }

        override fun onError(e: Throwable) {
            Timber.e(e,"Unexpected error")
        }

        override fun onComplete() {
        }
    }

    fun <T>observe(observable: Observable<T>, action:(t:T)->Unit) {
        var observer = AutoDisposingObserver(action)
        observable.subscribe(observer)
    }

    @Synchronized
    private fun callDeferredActions(api: Api) {
        for(action in actions) {
            action(api)
        }
        actions.clear()
    }

    @Synchronized
    fun whenReady(action:(api: Api)->Unit) {
        val api = api
        if (api ==null) {
            actions.add(action)
        } else {
            action(api)
        }
    }

    inner class Callback<T>(private var onResponseMethod: ((T)->Unit)?, private var onFailureMethod: ((throwable: Throwable)->Unit)?=null) : Disposable {
        init {
            Timber.d( "callback $hc created on $onResponseMethod")
            disposables.add(this)
        }
        fun onResponse(t:T) {
            Timber.d("calling callback $hc, disposed=${isDisposed()}")
            onResponseMethod?.let { it(t) }
        }
        fun onFailure(throwable: Throwable) {
            Timber.d("error on callback $hc, disposed=${isDisposed()}")
            onFailureMethod?.let {it(throwable)}
        }
        override fun isDisposed(): Boolean = onResponseMethod==null

        override fun dispose() {
            onResponseMethod = null
            onFailureMethod = null
            Timber.d("disposed callback $hc")
        }

        private val hc:String
            get() = System.identityHashCode(this).toString()+"/"+System.identityHashCode(this@HitchServiceConnection).toString()

    }
}
