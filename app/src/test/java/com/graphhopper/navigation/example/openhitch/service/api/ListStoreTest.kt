package com.graphhopper.navigation.example.openhitch.service.api

import com.graphhopper.navigation.example.openhitch.service.ListStore
import com.graphhopper.navigation.example.openhitch.service.testSubscribe
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class Thing(
    val id: String,
    val data: Int
)

class TestListStore : ListStore<Thing>() {
    override fun getKey(thing: Thing): String {
        return thing.id
    }
}

class ListStoreTest {
    val ID1 = "ID1"
    val ID2 = "ID2"
    val ID3 = "ID3"
    val ID4 = "ID4"
    val thing1 = Thing(ID1,1)
    val thing2 = Thing(ID2,2)
    val thing3 = Thing(ID3,3)
    val thing4 = Thing(ID4,4)
    val thing1_changed = Thing(ID1,11)
    val thing2_changed = Thing(ID2,22)
    val thing3_changed = Thing(ID3,33)

    lateinit var listStore:TestListStore

    @Before
    fun setUp() {
        listStore = TestListStore()
    }

    @Test
    fun putAndGet() {
        listStore.put(thing1)
        assertEquals(thing1,listStore.getItem(ID1))
        assertNull(listStore.getItem(ID2))
    }

    @Test
    fun remove() {
        listStore.put(thing1)
        listStore.put(thing2)
        assertEquals(thing1,listStore.getItem(ID1))
        assertEquals(thing2,listStore.getItem(ID2))
        listStore.remove(thing1.id)
        assertNull(listStore.getItem(ID1))
        assertEquals(thing2,listStore.getItem(ID2))
    }

    @Test
    fun putList() {
        listStore.putList(listOf(thing1,thing2))
        assertEquals(thing1,listStore.getItem(ID1))
        assertEquals(thing2,listStore.getItem(ID2))
    }

    @Test
    fun getCurrentItems() {
        listStore.putList(listOf(thing1,thing2))
        val currentItems = listStore.getCurrentItems()
        assertEquals(2,currentItems.size)
        assertTrue(currentItems.contains(thing1))
        assertTrue(currentItems.contains(thing2))
    }


    @Test
    fun putMap() {
        listStore.putMap(mapOf(ID1 to thing1, ID2 to thing2))
        checkContent(thing1,thing2)
        listStore.putMap(mapOf(ID2 to thing2_changed))
        checkContent(thing1,thing2_changed)
        listStore.putMap(mapOf(ID1 to null))
        checkContent(thing2_changed)
    }

    private fun checkContent(vararg things: Thing) {
        val currentItems = listStore.getCurrentItems()
        assertEquals(things.toSet(), currentItems.toSet())
    }

    @Test
    fun getObservable() {
        val testObserver = TestObserver<List<Thing>>()
        val observable = listStore.listStream { true }
        observable.subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe(testObserver)
        listStore.putList(listOf(thing1,thing2))
        listStore.remove(thing1.id)
        listStore.put(thing2_changed)
        listStore.putList(listOf(thing3))

        val values = testObserver.values()
        assertEquals(5,values.size)
        assertEquals(emptySet<Thing>(),values[0].toSet())
        assertEquals(setOf(thing1,thing2),values[1].toSet())
        assertEquals(setOf(thing2),values[2].toSet())
        assertEquals(setOf(thing2_changed),values[3].toSet())
        assertEquals(setOf(thing2_changed,thing3),values[4].toSet())
    }

    @Test
    fun getObservable_filtered() {
        val testObserver = TestObserver<List<Thing>>()
        val observable = listStore.listStream { it.data%2==0 }
        observable.subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe(testObserver)
        listStore.putList(listOf(thing1,thing2,thing3))
        listStore.remove(thing1.id)
        listStore.put(thing2_changed)
        listStore.put(thing3_changed)
        listStore.putList(listOf(thing4))
        listStore.remove(thing2_changed.id)

        val values = testObserver.values()
        assertEquals(5,values.size)
        assertEquals(emptySet<Thing>(),values[0].toSet())
        assertEquals(setOf(thing2),values[1].toSet())
        assertEquals(setOf(thing2_changed),values[2].toSet())
        assertEquals(setOf(thing2_changed,thing4),values[3].toSet())
        assertEquals(setOf(thing4),values[4].toSet())
    }

    @Test
    fun getObservable_lateSubscribe() {
        listStore.put(thing1)
        listStore.put(thing2)
        val testObserver = TestObserver<List<Thing>>()
        val observable = listStore.listStream {true}
        observable.subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe(testObserver)
        listStore.put(thing3)

        val values = testObserver.values()
        assertEquals(2,values.size)
        assertEquals(setOf(thing1,thing2),values[0].toSet())
        assertEquals(setOf(thing1,thing2,thing3),values[1].toSet())
    }

    @Test
    fun getItemObservable() {
        val testObserver = TestObserver<Thing>()
        listStore.itemStream(thing1.id).testSubscribe(testObserver)
        listStore.putList(arrayListOf(thing1))
        testObserver.assertValues(thing1)
    }
}